package pt.ulisboa.tecnico.cmu.shopist.store;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;

import static pt.ulisboa.tecnico.cmu.shopist.store.CreateStoreActivity.MY_COORDS;
import static pt.ulisboa.tecnico.cmu.shopist.store.CreateStoreActivity.STORE_COORDS;

public class StoreMapCreation extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Coordinates myCoords;
    private Coordinates storeCoords;
    private Marker marker;
    private FloatingActionButton save_btn;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_map_creation);

        myCoords = (Coordinates) getIntent().getSerializableExtra(MY_COORDS);
        storeCoords = (Coordinates)  getIntent().getSerializableExtra(STORE_COORDS);
        save_btn = findViewById(R.id.save_coords);
        searchView = findViewById(R.id.store_map_search);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // on below line we are getting the
                // location name from search view.
                String location = searchView.getQuery().toString();

                // below line is to create a list of address
                // where we will store the list of all address.
                List<Address> addressList = null;

                // checking if the entered location is null or not.
                if (location != null || location.equals("")) {
                    // on below line we are creating and initializing a geo coder.
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        // on below line we are getting location from the
                        // location name and adding that location to address list.
                        addressList = geocoder.getFromLocationName(location, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // on below line we are getting the location
                    // from our list a first position.
                    Address address = addressList.get(0);

                    // on below line we are creating a variable for our location
                    // where we will add our locations latitude and longitude.
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());


                    // below line is to animate camera to that position.
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);

        // If pantry location previously selected, load location
        if (storeCoords!=null) {
            LatLng pantryLoc = new LatLng(storeCoords.getLatitude(),storeCoords.getLongitude());
            marker = mMap.addMarker(new MarkerOptions().position(pantryLoc));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pantryLoc, 15.0f));
            save_btn.setEnabled(true);
        } else {
            LatLng myLoc = new LatLng(myCoords.getLatitude(), myCoords.getLongitude());
            marker = mMap.addMarker(new MarkerOptions().position(myLoc));
            marker.setVisible(false);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 15.0f));
        }

        marker.setTitle(getString(R.string.store_location));
        marker.showInfoWindow();


        // Set listener to update marker position on Map Click
        mMap.setOnMapClickListener(location -> {
            marker.setVisible(true);
            marker.setPosition(location);
            save_btn.setEnabled(true);
        });
    }

    public void saveCoords(View view) throws InvalidCoordinatesException {
        Coordinates storeCoords = new Coordinates(marker.getPosition().latitude,marker.getPosition().longitude);
        Intent returnIntent = new Intent();
        returnIntent.putExtra("coords",storeCoords);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


}