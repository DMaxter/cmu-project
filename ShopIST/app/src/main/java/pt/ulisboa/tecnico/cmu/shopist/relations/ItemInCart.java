package pt.ulisboa.tecnico.cmu.shopist.relations;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "cart",
        primaryKeys = {"item_name", "store_name"},
        foreignKeys = {
            @ForeignKey(entity = Item.class,
                        parentColumns = "item_name",
                        childColumns = "item_name",
                        onDelete = CASCADE),
            @ForeignKey(entity = Store.class,
                        parentColumns = "store_name",
                        childColumns = "store_name",
                        onDelete = CASCADE),
        })
public class ItemInCart {
    @NonNull
    @Embedded
    private final Item item;

    @NonNull
    @Embedded
    private final Store store;

    @ColumnInfo(name = "amount")
    private int amount;

    @Ignore
    private ItemToBuy toBuy;

    public ItemInCart(Store store, ItemToBuy item, int amount) {
        this.store = store;
        this.item = item.getStoreItem().getItem();
        this.amount = amount;
        this.toBuy = item;
    }

    public ItemInCart(Store store, Item item, int amount) {
        this.store = store;
        this.item = item;
        this.amount = amount;
    }

    public void addAmount(int amount) {
        this.amount += amount;
        this.toBuy.addAmount(-amount);
    }

    public String getName() {
        return item.getName();
    }

    public int getAmount() {
        return this.amount;
    }

    public Item getItem() {
        return this.item;
    }

    public Store getStore() {
        return store;
    }

    public double getPrice() {
        return toBuy.getPrice();
    }

    public void setItemToBuy(ItemToBuy toBuy) {
        this.toBuy = toBuy;
    }

    public ItemToBuy getItemToBuy() {
        return this.toBuy;
    }

    public void remove() {
        if (toBuy != null) {
            toBuy.setAmount(toBuy.getAmount() + this.amount);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemInCart that = (ItemInCart) o;
        return item.equals(that.item) &&
                store.equals(that.store);
    }
}
