package pt.ulisboa.tecnico.cmu.shopist.store;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import android.os.Messenger;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.inesc.termite.wifidirect.service.SimWifiP2pService;

public class QueueCommunicator {
    private static SimWifiP2pManager manager = null;
    private static QueueBroadcastReceiver receiver;
    private static ServiceConnection connection = null;
    private static SimWifiP2pManager.Channel channel = null;
    private static QueueListener listener = null;

    // Initialize data needed for the queue waiting time communication
    public static void init(Context context, Looper looper) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION);
        receiver = new QueueBroadcastReceiver();
        context.registerReceiver(receiver, filter);
        listener = new QueueListener();

        connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                manager = new SimWifiP2pManager(new Messenger(service));
                channel = manager.initialize(context, looper, null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                manager = null;
                channel = null;
            }
        };

        Intent intent = new Intent(context, SimWifiP2pService.class);
        context.bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    public static void checkPeers() {
        manager.requestPeers(channel, listener);
    }
}
