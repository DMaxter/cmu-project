package pt.ulisboa.tecnico.cmu.shopist.pantry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

public class ReceivePantry extends AppCompatActivity {
    private TextView shareCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_pantry);


        ImageView qrCode = findViewById(R.id.qrCode);
        shareCode = findViewById(R.id.share_code);

        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        String user;
        if (logged) {
            user = settings.getString(PREF_USER, "");
        }
        else {
            user = settings.getString(PREF_GUID, "");
        }


        generateQR(qrCode, user);
        showShareCode(user);
    }

    public void generateQR(ImageView qrView, String uuid) {
        try {
            Bitmap bm = encodeAsBitmap(uuid);
            if(bm != null) {
                qrView.setImageBitmap(bm);
            }
        } catch (WriterException e) {
            runOnUiThread( () -> {
                Toast.makeText(getApplicationContext(), "There was an error generating QR Code!", Toast.LENGTH_LONG).show();
            });
        }
    }

    // Default encondeAsBitmap function to turn a string into QR Code
    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, 150, 150, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    }

    public void showShareCode(String uuid) {
        shareCode.setText(uuid);
    }
}