package pt.ulisboa.tecnico.cmu.shopist.relations;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "store_item",
        primaryKeys = {"store_name", "item_name"},
        foreignKeys = {
            @ForeignKey(entity = Store.class,
                    parentColumns = {"store_name"},
                    childColumns = {"store_name"},
                    onDelete = CASCADE),
            @ForeignKey(entity = Item.class,
                    parentColumns = {"item_name"},
                    childColumns = {"item_name"},
                    onDelete = CASCADE)})
public class StoreItemCrossRef {
    @NonNull
    @ColumnInfo(name = "store_name")
    public String storeName;

    @NonNull
    @ColumnInfo(name = "item_name")
    public String itemName;

    @ColumnInfo(name = "price")
    public double price;
}
