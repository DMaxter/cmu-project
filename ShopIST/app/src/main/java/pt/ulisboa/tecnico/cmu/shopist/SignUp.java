package pt.ulisboa.tecnico.cmu.shopist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserAlreadyExistsException;

public class SignUp extends AppCompatActivity implements TextWatcher {
    private String GUID;
    private EditText usernameField;
    private EditText passwordField;
    private EditText confirmPasswordField;
    private Button signUpButton;
    private TextView notMatchingPasswords;
    private TextView duplicateUsernameText;
    private boolean error = false;
    private boolean duplicateUser = false;

    // Result constants
    public static final int RESULT_OK = 0;
    public static final int RESULT_NOK = 1;
    public static final String RESULT_USER_KEY = "username";
    public static final String RESULT_PASS_KEY = "password";
    public static final String RESULT_GUID = "Guid";
    public static final String INTENT_GUID = "Guid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Intent intent = getIntent();
        GUID = intent.getStringExtra(INTENT_GUID);

        // Get views
        signUpButton = findViewById(R.id.register_btn);
        signUpButton.setClickable(false);
        signUpButton.setEnabled(false);

        notMatchingPasswords = findViewById(R.id.not_matching_pwd);
        duplicateUsernameText = findViewById(R.id.duplicate_usr_txt);

        // Add TextWatchers to 3 fields
        usernameField = findViewById(R.id.signup_username);
        usernameField.addTextChangedListener(this);
        passwordField = findViewById(R.id.signup_password);
        passwordField.addTextChangedListener(this);
        confirmPasswordField = findViewById(R.id.signup_password2);
        confirmPasswordField.addTextChangedListener(this);
    }

    public void registerUser(View view) {
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        String confirmPassword = confirmPasswordField.getText().toString();
        if (password.equals(confirmPassword)) {
            new Thread(() -> {
                try {
                    RemoteOperations.register(username, GUID, MainActivity.hash(password));
                    new Thread(() -> Database.registerOwner(GUID, username)).start();
                    runOnUiThread(() -> {
                        Intent intent = new Intent();
                        intent.putExtra(RESULT_USER_KEY, username);
                        intent.putExtra(RESULT_PASS_KEY, password);
                        intent.putExtra(RESULT_GUID, GUID);
                        setResult(RESULT_OK, intent);
                        finish();
                    });
                } catch (UserAlreadyExistsException e) {
                    runOnUiThread(() -> {
                        duplicateUser = true;
                        usernameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                        duplicateUsernameText.setVisibility(View.VISIBLE);

                    });
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                    );
                }
            }).start();
        } else {
            error = true;
            passwordField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            confirmPasswordField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            notMatchingPasswords.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String password = passwordField.getText().toString();
        String confirmPassword = confirmPasswordField.getText().toString();

        // Verify is button should be clickable
        if (password.length() > 0 && confirmPassword.length() > 0) {
            signUpButton.setClickable(true);
            signUpButton.setEnabled(true);

        } else {
            signUpButton.setClickable(false);
            signUpButton.setEnabled(false);
        }

        // Verify if user is correcting previous mismatched password
        if (error) {
            error = false;
            passwordField.getBackground().mutate().setColorFilter(null);
            confirmPasswordField.getBackground().mutate().setColorFilter(null);
            notMatchingPasswords.setVisibility(View.INVISIBLE);
        }

        // Verify if user is correcting duplicate username
        if (duplicateUser) {
            duplicateUser = false;
            usernameField.getBackground().mutate().setColorFilter(null);
            duplicateUsernameText.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_NOK);
        finish();
    }
}