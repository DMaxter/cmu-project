package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.CoordinatesDto;

public class CoordinatesDtoJsonSerializer implements JsonSerializer<CoordinatesDto> {
    @Override
    public JsonElement serialize(CoordinatesDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("latitude", context.serialize(dto.getLatitude()));
        obj.add("longitude", context.serialize(dto.getLongitude()));

        return obj;
    }

    public static String serialize(CoordinatesDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}