package pt.ulisboa.tecnico.cmu.shopist.dto;

public class AmountDto {
    private final int amountPantry;
    private final int amountShop;

    public AmountDto(int pantry, int shop) {
        this.amountPantry = pantry;
        this.amountShop = shop;
    }

    public int getAmountPantry() {
        return this.amountPantry;
    }

    public int getAmountShop() {
        return this.amountShop;
    }
}
