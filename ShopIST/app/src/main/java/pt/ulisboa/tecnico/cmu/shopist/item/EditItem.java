package pt.ulisboa.tecnico.cmu.shopist.item;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.relations.PantryItemCrossRef;

public class EditItem extends AppCompatActivity implements TextWatcher {
    private EditText pantryAmountField;
    private EditText shopAmountField;
    private TextView invalidPantryAmountText;
    private TextView invalidShopAmountText;
    private String itemName;
    private ItemInPantryList item;
    private boolean shopAmountError = false;
    private boolean pantryAmountError = false;

    public static final String RESULT_ITEM_NAME = "ResultName";
    public static final String RESULT_ITEM_AMOUNT = "ResultAmount";

    public static final String INTENT_ITEM_NAME = "ItemName";
    public static final String INTENT_ITEM_AMOUNT = "ItemAmount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        Intent intent = getIntent();
        itemName = intent.getStringExtra(INTENT_ITEM_NAME);
        item = (ItemInPantryList) intent.getSerializableExtra(INTENT_ITEM_AMOUNT);

        TextView itemNameText = findViewById(R.id.edit_item_txt);
        String title = "Edit " + itemName;
        itemNameText.setText(title);
        pantryAmountField = findViewById(R.id.edit_item_pantry);
        pantryAmountField.setText(String.valueOf(item.getAmountPantry()));
        pantryAmountField.addTextChangedListener(this);
        shopAmountField = findViewById(R.id.edit_item_shop);
        shopAmountField.setText(String.valueOf(item.getAmountShop()));
        shopAmountField.addTextChangedListener(this);
        invalidPantryAmountText = findViewById(R.id.edit_item_invalid_pantry);
        invalidShopAmountText = findViewById(R.id.edit_item_invalid_shop);
    }

    public void editItem(View view) {
        int pantryAmount, shopAmount;
        if (pantryAmountField.getText().toString().equals("")) {
            pantryAmountError = true;
            invalidPantryAmountText.setVisibility(View.VISIBLE);
            pantryAmountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            return;
        } else {
            try {
                pantryAmount = Integer.parseInt(pantryAmountField.getText().toString());
                if (pantryAmount < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                pantryAmountError = true;
                invalidPantryAmountText.setVisibility(View.VISIBLE);
                pantryAmountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                return;
            }
        }

        if (shopAmountField.getText().toString().equals("")) {
            shopAmountError = true;
            invalidShopAmountText.setVisibility(View.VISIBLE);
            shopAmountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            return;
        } else {
            try {
                shopAmount = Integer.parseInt(shopAmountField.getText().toString());
                if (shopAmount < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                shopAmountError = true;
                invalidShopAmountText.setVisibility(View.VISIBLE);
                shopAmountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                return;
            }
        }

        if (!shopAmountError && !pantryAmountError) {
            Intent intent = new Intent();
            item.setAmountPantry(pantryAmount);
            item.setAmountShop(shopAmount);
            intent.putExtra(RESULT_ITEM_AMOUNT, item);
            intent.putExtra(RESULT_ITEM_NAME, itemName);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    public void readBarcode(View view) {
        IntentIntegrator barcodeScan = new IntentIntegrator(this);
        barcodeScan.setOrientationLocked(false);
        barcodeScan.addExtra("SCAN_FORMATS","EAN_13");
        barcodeScan.initiateScan();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (pantryAmountError) {
            pantryAmountError = false;
            invalidPantryAmountText.setVisibility(View.INVISIBLE);
            pantryAmountField.getBackground().mutate().setColorFilter(null);
        }

        if (shopAmountError) {
            shopAmountError = false;
            invalidShopAmountText.setVisibility(View.INVISIBLE);
            shopAmountField.getBackground().mutate().setColorFilter(null);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
