package pt.ulisboa.tecnico.cmu.shopist.item;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.relations.PantryItemCrossRef;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;

public class CreateItem extends AppCompatActivity implements TextWatcher {
    private TextView barcodeText;
    private EditText itemNameField;
    private EditText amountField;
    private TextView noNameText;
    private TextView invalidAmountText;
    private ImageView itemImage;
    private String barcode = "";
    private boolean nameError = false;
    private boolean amountError = false;
    private Bitmap imageBitmap = null;

    public static final String RESULT_ITEM = "Item";
    public static final String RESULT_ITEM_AMOUNT_PANTRY = "ItemAmountPantry";
    public static final String RESULT_ITEM_AMOUNT_SHOP = "ItemAmountShop";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_BARCODE = 49374; // Pre-defined in library

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        itemImage = findViewById(R.id.item_img);
        barcodeText = findViewById(R.id.barcode_txt);
        itemNameField = findViewById(R.id.edit_item_pantry);
        itemNameField.addTextChangedListener(this);
        amountField = findViewById(R.id.edit_item_shop);
        amountField.addTextChangedListener(this);
        noNameText = findViewById(R.id.edit_item_invalid_pantry);
        invalidAmountText = findViewById(R.id.create_item_invalid_amount);
    }

    public void createItem(View view) {
        String name = itemNameField.getText().toString();

        if (name.equals("")) {
            nameError = true;
            noNameText.setVisibility(View.VISIBLE);
            itemNameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
        }

        int amount;
        if (amountField.getText().toString().equals("") || amountField.getText().toString().equals("0")) {
            amountError = true;
            invalidAmountText.setVisibility(View.VISIBLE);
            amountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            return;
        } else {
            try {
                amount = Integer.parseInt(amountField.getText().toString());
                if (amount < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                amountError = true;
                invalidAmountText.setVisibility(View.VISIBLE);
                amountField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                return;
            }
        }

        if (!nameError && !amountError) {
            Intent intent = new Intent();
            Item item = new Item(name, barcode);
            intent.putExtra(RESULT_ITEM, item);
            intent.putExtra(RESULT_ITEM_AMOUNT_PANTRY, amount);
            intent.putExtra(RESULT_ITEM_AMOUNT_SHOP, 0);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    public void readBarcode(View view) {
        IntentIntegrator barcodeScan = new IntentIntegrator(this);
        barcodeScan.setOrientationLocked(false);
        barcodeScan.addExtra("SCAN_FORMATS", "EAN_13");
        barcodeScan.initiateScan();
    }

    public void takePicture(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            itemImage.setImageBitmap(imageBitmap);
            Log.i("IMAGE", "Content is " + imageBitmap);

        } else if (requestCode == REQUEST_BARCODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode,
                    resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Context context = getApplicationContext();
                    CharSequence text = "Error reading item's barcode...";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                } else {
                    barcode = result.getContents();
                    barcodeText.setText(barcode);

                    Log.i("BARCODE", barcode);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (nameError && !itemNameField.getText().toString().equals("")) {
            nameError = false;
            noNameText.setVisibility(View.INVISIBLE);
            itemNameField.getBackground().mutate().setColorFilter(null);
        }

        if (amountError) {
            amountError = false;
            invalidAmountText.setVisibility(View.INVISIBLE);
            amountField.getBackground().mutate().setColorFilter(null);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}