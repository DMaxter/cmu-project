package pt.ulisboa.tecnico.cmu.shopist.relations;

import androidx.room.Embedded;

import java.util.Objects;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;

public class ItemToBuy {
    @Embedded
    private ItemInStore item;

    private int amount;

    public ItemToBuy(ItemInStore item, int amount) {
        this.item = item;
        this.amount = amount;
    }

    public ItemInStore getStoreItem() {
        return item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Item getItem() {
        return item.getItem();
    }

    public String getName() {
        return item.getItem().getName();
    }

    public double getPrice() {
        return item.getPrice();
    }

    public void setPrice(double price) { item.setPrice(price);}

    public void addAmount(int amount) {
        this.amount += amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemToBuy itemToBuy = (ItemToBuy) o;
        return Objects.equals(item, itemToBuy.item);
    }
}
