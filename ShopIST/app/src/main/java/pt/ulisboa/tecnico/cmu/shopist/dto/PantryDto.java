package pt.ulisboa.tecnico.cmu.shopist.dto;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.relations.PantryItemCrossRef;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

public class PantryDto {
    private final String id;
    private final String name;
    private final CoordinatesDto coords;
    private final TreeMap<String, AmountDto> items;

    public PantryDto(PantryList pantry, List<ItemInPantryList> list) {
        this.id = pantry.getId().toString();
        this.name = pantry.getName();
        this.coords = new CoordinatesDto(pantry.getCoords());
        this.items = new TreeMap<>();

        for(ItemInPantryList item: list) {
            this.items.put(item.getItem().getName(), new AmountDto(item.getAmountPantry(), item.getAmountShop()));
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CoordinatesDto getCoords() {
        return coords;
    }

    public TreeMap<String, AmountDto> getItems() {
        return items;
    }

}
