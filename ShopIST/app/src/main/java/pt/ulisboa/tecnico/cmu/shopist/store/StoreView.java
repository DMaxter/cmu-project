package pt.ulisboa.tecnico.cmu.shopist.store;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Cart;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

public class StoreView extends AppCompatActivity {

    ListView listView;
    List<ItemToBuy> list;
    Cart cart;
    Store store;
    String user;
    TabLayout tabLayout;
    ExtendedFloatingActionButton checkout;
    public static int EDIT_STORE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_view);

        LocalBroadcastManager.getInstance(this).registerReceiver(updateReceiver, new IntentFilter("updateCartTotal"));
        store = (Store) getIntent().getSerializableExtra("store");
        this.setTitle(store.getName());

        cart = new Cart(store);
        new Thread(() -> {
            getStoreView();
        }).start();
        listView = findViewById(R.id.list_view);
        checkout = findViewById(R.id.checkout);


        tabLayout = findViewById(R.id.tabLayout3);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    //STORE
                    showStore();
                } else {
                    //CART
                    showCart();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, EditStore.class);
        intent.putExtra("storeToEdit", store);
        startActivityForResult(intent,EDIT_STORE);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.store_settings_menu, menu);
        return true;
    }

    public void getStoreView() {

        //Get user info
        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        if (logged) {
            user = settings.getString(PREF_USER, "");
        } else {
            user = settings.getString(PREF_GUID, "");
        }

        //Get items filtered items from server
        try {
            list = Database.getItemsToBuy(store.getName());
            List<ItemToBuy> newList = RemoteOperations.getStoreView(user, store);
            addStoreItems(newList.stream().filter(e -> !list.contains(e)).map(ItemToBuy::getStoreItem).collect(Collectors.toList()));
            Map<String, ItemToBuy> itemsToBuy = newList.stream().collect(Collectors.toMap(ItemToBuy::getName, Function.identity()));
            List<ItemInStore> updateList = list.stream()
                    .filter(e -> itemsToBuy.containsKey(e.getName()) &&
                            e.getPrice() != itemsToBuy.get(e.getName()).getPrice())
                    .map(ItemToBuy::getStoreItem)
                    .collect(Collectors.toList());
            updateStoreItems(updateList);

            // Check if prices changed
            if (!updateList.isEmpty()) {
                cart.clean();
            } else {
                // Check if cart amounts are still fine
                List<ItemInCart> remaining = cart.getItems().stream().filter(e -> {
                    if (itemsToBuy.containsKey(e.getName())) {
                        ItemToBuy item = itemsToBuy.get(e.getName());
                        if (e.getItemToBuy() == null) {
                            e.setItemToBuy(item);
                        }

                        int cartAmount = e.getAmount();
                        int itemAmount = item.getAmount();
                        if (cartAmount >= itemAmount) {
                            e.addAmount(itemAmount - cartAmount);
                            item.setAmount(0);
                            newList.remove(item);
                        } else {
                            item.setAmount(itemAmount - cartAmount);
                        }
                        return true;
                    } else {
                        return false;
                    }
                }).collect(Collectors.toList());

                cart.update(remaining);

                runOnUiThread(() -> {
                    @SuppressLint("DefaultLocale") String total = String.format("%.2f €", cart.getTotalCost());
                    checkout.setText(total);
                });
            }

            list = newList;
        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        }

        runOnUiThread(this::showStore);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDIT_STORE) {
                new Thread(this::getStoreView).start();
            }
        }
    }

    public void showStore() {
        ArrayAdapter<ItemToBuy> StoreItemAdapter = new StoreItemAdapter(this, list, cart);
        listView.setAdapter(StoreItemAdapter);
        checkout.setVisibility(View.INVISIBLE);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            Object clicked = parent.getItemAtPosition(position);
            if (clicked instanceof ItemInCart) {
                ItemInCart item = (ItemInCart) clicked;
                String text = getString(R.string.wow) + " " + item.getName() + " " + getString(R.string.for_string) + " " + item.getPrice() + " " + getString(R.string.at) + " " + store.getName();
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
            } else {
                ItemToBuy item = (ItemToBuy) clicked;
                String text = getString(R.string.wow) + " " + item.getName() + " " + getString(R.string.for_string) + " " + item.getPrice() + " " + getString(R.string.at) + " " + store.getName();
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
            }

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
        });
    }

    public void showCart() {
        ArrayAdapter<ItemInCart> CartItemAdapter = new CartItemAdapter(this, cart, cart.getItems());
        listView.setAdapter(CartItemAdapter);
        checkout.setVisibility(View.VISIBLE);
    }

    public void checkoutStore(View view) {
        new Thread(() -> {
            try {
                RemoteOperations.checkoutStore(store.getName(), user, cart.getItemsInCart());
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            }
            cart.clean();
        }).start();

        setResult(Activity.RESULT_OK, new Intent());
        finish();
    }

    public BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Double newTotal = (Double) intent.getSerializableExtra("newTotal");
            @SuppressLint("DefaultLocale") String total = String.format("%.2f €", newTotal);
            checkout.setText(total);
        }
    };

    private void addStoreItems(List<ItemInStore> list) {
        list.forEach(e -> Database.addItemToStore(e.getStore().getName(), e.getName(), e.getPrice()));
    }

    private void updateStoreItems(List<ItemInStore> list) {
        list.forEach(e -> Database.updateStoreItem(e.getStore().getName(), e.getName(), e.getPrice()));
    }
}