package pt.ulisboa.tecnico.cmu.shopist.pantry;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

public class PantryMenuAdapter extends ArrayAdapter<PantryList> {
    private final Context context;
    private final List<PantryList> values;
    private String user;

    public PantryMenuAdapter(Context context, List<PantryList> values, String user) {
        super(context, R.layout.adapter_pantry_list_menu, values);
        this.context = context;
        this.values = values;
        this.user = user;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_pantry_list_menu, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView coordsView = rowView.findViewById(R.id.secondLine);
        ImageView imageView = rowView.findViewById(R.id.icon);

        if (!user.equals(values.get(position).getOwner())) {
            String sharedName = values.get(position).getName() + " ("+ context.getString(R.string.shared_by) +" "+ values.get(position).getOwner() + ")";
            nameView.setText(sharedName);
        } else {
            nameView.setText(values.get(position).getName());
        }

        coordsView.setText(values.get(position).getCoords().toString());
        class DirectionsRunnable implements Runnable {
            JSONObject response;

            @Override
            public void run() {
                try {
                    URL url = new URL("https://maps.googleapis.com/maps/api/directions/json?origin=" + values.get(position).getCoords().getLatitude() + "," + values.get(position).getCoords().getLongitude() + "&destination=" + values.get(position).getCoords().getLatitude() + "," + values.get(position).getCoords().getLongitude() + "&key=AIzaSyAy6vSql-qRt9yvqBzejSWMl5hMfR5pxdk");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                    StringBuilder sb = new StringBuilder();
                    String str;
                    while ((str = reader.readLine()) != null) {
                        sb.append(str);
                    }
                    response = new JSONObject(sb.toString());

                } catch (IllegalStateException | IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            public JSONObject getResponse() {
                return response;
            }
        }
        DirectionsRunnable dr = new DirectionsRunnable();
        Thread t = new Thread(dr);
        t.start();
        try {
            t.join();
            coordsView.setText(dr.getResponse().getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("start_address"));
        } catch (InterruptedException | JSONException e) {
            e.printStackTrace();
        }

        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        return rowView;
    }

    public PantryList getItem(int position){
        return values.get(position);
    }
}
