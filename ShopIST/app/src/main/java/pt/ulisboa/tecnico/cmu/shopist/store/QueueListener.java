package pt.ulisboa.tecnico.cmu.shopist.store;

import java.util.List;

import pt.inesc.termite.wifidirect.SimWifiP2pDevice;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.MainMenu;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;

public class QueueListener implements SimWifiP2pManager.PeerListListener {
    private boolean inStore = false;

    private static final String BEACON_STORE_ID = "B";
    private static String last;

    @Override
    public void onPeersAvailable(SimWifiP2pDeviceList peers) {
        new Thread(() -> {
            SimWifiP2pDevice device = peers.getByName(BEACON_STORE_ID);
            List<ItemInCart> items = Database.getCart();
            if (device != null && !inStore) {
                if (items.size() == 0) {
                    return;
                }

                last = items.get(0).getStore().getName();
                try {
                    RemoteOperations.enterQueue(last, MainMenu.user, items.size());
                } catch (ServerUnreachableException e) {
                    e.printStackTrace();
                }
                inStore = true;
            } else if (device == null && inStore) {
                String name = items.size() == 0 ? last : items.get(0).getStore().getName();
                try {
                    RemoteOperations.leaveQueue(name, MainMenu.user);
                } catch (ServerUnreachableException e) {
                    e.printStackTrace();
                }
                inStore = false;
            }
        }).start();
    }
}
