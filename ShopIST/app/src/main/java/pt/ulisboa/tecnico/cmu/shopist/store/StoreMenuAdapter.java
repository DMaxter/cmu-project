package pt.ulisboa.tecnico.cmu.shopist.store;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;

public class StoreMenuAdapter extends ArrayAdapter<Store> {
    private final Context context;
    private final List<Store> values;
    private final Location location;

    public StoreMenuAdapter(Context context, List<Store> values, Location loc) {
        super(context, R.layout.adapter_store_menu, values);
        this.context = context;
        this.values = values;
        this.location = loc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_store_menu, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView distanceView = rowView.findViewById(R.id.secondLine);
        TextView waitView = rowView.findViewById(R.id.waitTime);
        ImageView imageView = rowView.findViewById(R.id.icon);

        long time = values.get(position).getTime();
        if (time == -1) {
            waitView.setVisibility(View.INVISIBLE);
        } else {
            waitView.setVisibility(View.VISIBLE);
            waitView.setText(time / 60000 + " min");
        }

        nameView.setText(values.get(position).getName());
        if (location != null) {
            class DirectionsRunnable implements Runnable {
                JSONObject response;

                @Override
                public void run() {
                    try {
                        URL url = new URL("https://maps.googleapis.com/maps/api/directions/json?origin=" + location.getLatitude() + "," + location.getLongitude() + "&destination=" + values.get(position).getCoords().getLatitude() + "," + values.get(position).getCoords().getLongitude() + "&key=AIzaSyAy6vSql-qRt9yvqBzejSWMl5hMfR5pxdk");
                        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuilder sb = new StringBuilder();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            sb.append(str);
                        }
                        response = new JSONObject(sb.toString());

                    } catch (IllegalStateException | IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

                public JSONObject getResponse() {
                    return response;
                }
            }
            DirectionsRunnable dr = new DirectionsRunnable();
            Thread t = new Thread(dr);
            t.start();
            try {
                t.join();
                distanceView.setText(dr.getResponse().getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("duration").getString("text"));
            } catch (InterruptedException | JSONException e) {
                e.printStackTrace();
            }
        }
        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        return rowView;
    }

    public Store getItem(int position) {
        return values.get(position);
    }
}
