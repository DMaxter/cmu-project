package pt.ulisboa.tecnico.cmu.shopist.relations;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

import java.io.Serializable;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

public class ItemInPantryList implements Serializable {
    @Embedded
    private PantryList pantry;

    @Embedded
    private Item item;

    @ColumnInfo(name = "amount_pantry")
    private int amountPantry;

    @ColumnInfo(name = "amount_shop")
    private int amountShop;

    public ItemInPantryList(PantryList pantry, Item item, int amountPantry, int amountShop) {
        this.pantry = pantry;
        this.item = item;
        this.amountPantry = amountPantry;
        this.amountShop = amountShop;
    }

    public PantryList getPantry() {
        return pantry;
    }

    public void setPantry(PantryList pantry) {
        this.pantry = pantry;
    }

    public int increaseAmount() {
        return ++this.amountPantry;
    }

    public int decreaseAmount() {
        this.amountShop++;
        return --this.amountPantry;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getAmountPantry() {
        return amountPantry;
    }

    public void setAmountPantry(int amountPantry) {
        this.amountPantry = amountPantry;
    }

    public int getAmountShop() {
        return amountShop;
    }

    public void setAmountShop(int amountShop) {
        this.amountShop = amountShop;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemInPantryList that = (ItemInPantryList) o;
        return pantry.equals(that.pantry) && item.equals(that.item);
    }
}
