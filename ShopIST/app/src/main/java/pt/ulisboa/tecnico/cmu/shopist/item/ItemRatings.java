package pt.ulisboa.tecnico.cmu.shopist.item;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;


public class ItemRatings extends AppCompatActivity {

    // variable for our bar chart
    BarChart barChart;

    // variable for our bar data.
    BarData barData;

    // variable for our bar data set.
    BarDataSet barDataSet;

    // array list for storing entries.
    ArrayList barEntriesArrayList;

    RatingBar ratingBar;

    FloatingActionButton rate_button;

    private String itemName;

    private String username;

    private List<Integer> ratings;

    private Boolean canRate;

    public static final String INTENT_ITEM_NAME = "ItemName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_ratings);

        Intent intent = getIntent();
        itemName = intent.getStringExtra(INTENT_ITEM_NAME);

        // initializing variable for bar chart.
        barChart = findViewById(R.id.idBarChart);

        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        if (logged) {
            username = settings.getString(PREF_USER, "");
        } else {
            username = settings.getString(PREF_GUID, "");
        }

        //Get ratings
        try {
            ratings = RemoteOperations.getItemRatings(itemName);
        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        }

        //Get ratings
        try {
            canRate = RemoteOperations.canRateItem(username, itemName);
        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        }

        // calling method to get bar entries.
        getBarEntries();

        // creating a new bar data set.
        barDataSet = new BarDataSet(barEntriesArrayList, getString(R.string.ratings_of) +" "+ itemName);

        // creating a new bar data and
        // passing our bar data set.
        barData = new BarData(barDataSet);

        // below line is to set data
        // to our bar chart.
        barChart.setData(barData);

        YAxis left = barChart.getAxisLeft();
        left.setDrawLabels(false);
        left.setAxisMinimum(0);
        YAxis right = barChart.getAxisRight();
        right.setDrawLabels(false);
        right.setAxisMinimum(0);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawLabels(false);

        // adding color to our bar data set.
        barDataSet.setColors(Color.parseColor("#F78B5D"), Color.parseColor("#FCB232"), Color.parseColor("#FDD930"), Color.parseColor("#ADD137"), Color.parseColor("#A0C25A"));

        // setting text color.
        barDataSet.setValueTextColor(Color.BLACK);

        // setting text size
        barDataSet.setValueTextSize(16f);
        barChart.getDescription().setEnabled(false);

        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setStepSize(1);

        rate_button = findViewById(R.id.rate_button);

        rate_button.setOnClickListener(v -> {
            rate_button.setVisibility(View.INVISIBLE);
            ratingBar.setVisibility(View.INVISIBLE);
            findViewById(R.id.rate_item).setVisibility(View.INVISIBLE);
            findViewById(R.id.item_rated).setVisibility(View.VISIBLE);
            rateItem();
        });

        ratingBar.setOnRatingBarChangeListener( (x,y,z) -> rate_button.setEnabled(true));

        if (canRate) {
            ratingBar.setVisibility(View.VISIBLE);
            findViewById(R.id.rate_item).setVisibility(View.VISIBLE);
            findViewById(R.id.item_rated).setVisibility(View.INVISIBLE);
        }
        else {
            rate_button.setVisibility(View.INVISIBLE);
            ratingBar.setVisibility(View.INVISIBLE);
            findViewById(R.id.rate_item).setVisibility(View.INVISIBLE);
            findViewById(R.id.item_rated).setVisibility(View.VISIBLE);
        }
    }

    private void getBarEntries() {
        // creating a new array list
        barEntriesArrayList = new ArrayList<>();

        HashMap<Integer, Integer> ratings_counts = new HashMap<>();
        ratings_counts.put(1, 0);
        ratings_counts.put(2, 0);
        ratings_counts.put(3, 0);
        ratings_counts.put(4, 0);
        ratings_counts.put(5, 0);

        int average_rating = 0;
        for (Integer i: ratings) {
            ratings_counts.put(i, ratings_counts.get(i) + 1);
            average_rating += i;
        }

        TextView average_rating_text = findViewById(R.id.average_rating);
        String avg;
        if (average_rating == 0) {
            avg = getString(R.string.avg_rating) + " " + average_rating;
        }
        else {
            avg = getString(R.string.avg_rating) + " " + average_rating/ratings.size();
        }

        average_rating_text.setText(avg);

        for (Map.Entry<Integer, Integer> entry: ratings_counts.entrySet()) {
            barEntriesArrayList.add(new BarEntry(entry.getKey(), entry.getValue()));
        }
    }

    private void rateItem() {
        //Get ratings
        try {
            RemoteOperations.rateItem(username, itemName, (int) ratingBar.getRating());

            finish();
            startActivity(getIntent());

        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        }
    }
}