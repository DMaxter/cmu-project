package pt.ulisboa.tecnico.cmu.shopist.store;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Cart;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserNotOwnerException;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

public class CreateStoreItemAdapter extends ArrayAdapter<Item> {
    private final Context context;
    private final List<Item> values;
    private final HashMap<String, Double> checked;

    public CreateStoreItemAdapter(Context context, List<Item> values) {
        super(context, R.layout.adapter_store_items, values);
        this.context = context;
        this.values = values;
        this.checked = new HashMap<>();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_create_store_items, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView barcodeView = rowView.findViewById(R.id.secondLine);
        ImageView imageView = rowView.findViewById(R.id.icon);

        TextView priceView = rowView.findViewById(R.id.price);


        CheckBox item_check = rowView.findViewById(R.id.item_check);
        item_check.setChecked(false);

        item_check.setTag(values.get(position));
        item_check.setOnClickListener(v -> {
            if (!item_check.isChecked()) {
                checked.remove(values.get(position).getName());
                priceView.setText("");
            }
            else {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.price_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView.findViewById(R.id.input_price_field);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                (dialog, id) -> {
                                    // get user input and set it to result
                                    // edit text
                                    Double price;
                                    if (userInput.getText().toString().equals("")) {
                                        price = 0.0;
                                        priceView.setText("0 €");
                                    } else {
                                        price = Double.parseDouble(userInput.getText().toString());
                                        priceView.setText(userInput.getText() + " €");
                                    }
                                    checked.put(values.get(position).getName(), price);
                                })
                        .setNegativeButton(R.string.cancel,
                                (dialog, id) -> {
                                    item_check.setChecked(false);
                                    dialog.cancel();
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        nameView.setText(values.get(position).getName());
        barcodeView.setText(String.valueOf(values.get(position).getBarcode()));
        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        return rowView;
    }

    public Item getItem(int position) {
        return values.get(position);
    }

    public HashMap<String,Double> getCheckedItems() {
        return checked;
    }

}
