package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.CoordinatesDto;
import pt.ulisboa.tecnico.cmu.shopist.dto.ListStoresDto;

public class ListStoresDtoJsonSerializer implements JsonSerializer<ListStoresDto> {
    @Override
    public JsonElement serialize(ListStoresDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("shops", context.serialize(dto.getShops()));

        return obj;
    }

    public static String serialize(ListStoresDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(ListStoresDto.class, new ListStoresDtoJsonSerializer())
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
