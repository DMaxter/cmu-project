package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ItemDao {
    @Insert
    void insert(Item item);

    @Insert
    void insertAll(List<Item> items);

    @Update
    void update(Item item);

    @Update
    void updateAll(List<Item> item);

    @Query("SELECT * FROM item WHERE item_name = :name")
    Item getItem(String name);

    @Query("SELECT * FROM item ORDER BY item_name")
    List<Item> getAll();
}
