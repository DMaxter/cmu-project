package pt.ulisboa.tecnico.cmu.shopist.relations;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import java.io.Serializable;
import java.util.UUID;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "pantry_item",
        primaryKeys = {"item_name", "pantry_id"},
        foreignKeys = {
            @ForeignKey(entity = PantryList.class,
                    parentColumns = {"pantry_id"},
                    childColumns = {"pantry_id"},
                    onDelete = CASCADE),
            @ForeignKey(entity = Item.class,
                    parentColumns = {"item_name"},
                    childColumns = {"item_name"},
                    onDelete = CASCADE)})
public class PantryItemCrossRef {
    @NonNull
    @ColumnInfo(name = "pantry_id")
    public UUID pantryId;

    @NonNull
    @ColumnInfo(name = "item_name")
    public String itemId;

    @ColumnInfo(name = "amount_pantry")
    public int amountPantry;

    @ColumnInfo(name = "amount_shop")
    public int amountShop;
}
