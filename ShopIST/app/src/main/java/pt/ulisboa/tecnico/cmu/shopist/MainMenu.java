package pt.ulisboa.tecnico.cmu.shopist;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.dto.ListPantriesDto;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.AlreadyUpdatedException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.pantry.PantryDetails;
import pt.ulisboa.tecnico.cmu.shopist.pantry.PantryMenu;
import pt.ulisboa.tecnico.cmu.shopist.serializer.ListPantriesDtoJsonSerializer;
import pt.ulisboa.tecnico.cmu.shopist.store.StoreMenu;
import pt.ulisboa.tecnico.cmu.shopist.store.StoreView;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOCALE;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOCALE_DEFAULT;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOCALE_PT;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;
import static pt.ulisboa.tecnico.cmu.shopist.pantry.PantryMenu.VIEW_DETAILS;
import static pt.ulisboa.tecnico.cmu.shopist.store.StoreMenu.STORE_VIEW;

public class MainMenu extends AppCompatActivity {
    private final List<PantryList> pantry = new ArrayList<>();
    private final Map<String, Store> shops = new HashMap<>();
    private LocationManager locationManager;
    private FusedLocationProviderClient fusedLocationClient;
    private ArrayList<Store> listShops = new ArrayList<>();
    private List<PantryList> listPantries;
    public static String user;

    public static final String LOGOUT_KEY = "Logout";
    private final int MAX_DIST = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        getUser();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        } else {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            try {
                                listShops = (ArrayList<Store>) RemoteOperations.getStores(user);
                                if (getClosestShop(listShops, location)) {
                                    Thread t = new Thread(() -> {
                                        listPantries = Database.getPantryLists();
                                        String user = getUser();
                                        try {
                                            List<PantryList> newList = RemoteOperations.getPantryLists(user, Hasher.hash(ListPantriesDtoJsonSerializer.serialize(new ListPantriesDto(listPantries))));
                                            listPantries = newList;
                                        } catch (AlreadyUpdatedException ignored) {
                                        } catch (InvalidListException e) {
                                            runOnUiThread(() ->
                                                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                                            );
                                        } catch (ServerUnreachableException e) {
                                            runOnUiThread(() ->
                                                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                                            );
                                        }
                                    });
                                    t.start();
                                    try {
                                        t.join();
                                        getClosestPantry(listPantries, location);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            } catch (InvalidCoordinatesException e) {
                                runOnUiThread(() ->
                                        Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                                );
                            } catch (ServerUnreachableException e) {
                                runOnUiThread(() ->
                                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                                );
                            }
                        }
                    });
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            try {
                                listShops = (ArrayList<Store>) RemoteOperations.getStores(MainMenu.user);
                                if (getClosestShop(listShops, location)) {
                                    Thread t = new Thread(() -> {
                                        listPantries = Database.getPantryLists();
                                        String user = getUser();
                                        try {
                                            listPantries = RemoteOperations.getPantryLists(user, Hasher.hash(ListPantriesDtoJsonSerializer.serialize(new ListPantriesDto(listPantries))));
                                        } catch (AlreadyUpdatedException ignored) {
                                        } catch (InvalidListException e) {
                                            runOnUiThread(() ->
                                                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                                            );
                                        } catch (ServerUnreachableException e) {
                                            runOnUiThread(() ->
                                                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                                            );
                                        }
                                    });
                                    t.start();
                                    try {
                                        t.join();
                                        getClosestPantry(listPantries, location);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            } catch (InvalidCoordinatesException e) {
                                runOnUiThread(() ->
                                        Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                                );
                            } catch( ServerUnreachableException e) {
                                runOnUiThread(() ->
                                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                                );
                            }
                        }
                    });
        }
    }

    public String getUser() {
        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        if (logged) {
            user = settings.getString(PREF_USER, "");
        } else {
            user = settings.getString(PREF_GUID, "");
        }
        return user;
    }

    public boolean getClosestShop(ArrayList<Store> list, Location location) {
        for (Store store : list) {
            float[] results = new float[1];
            Location.distanceBetween(store.getCoords().getLatitude(), store.getCoords().getLongitude(), location.getLatitude(), location.getLongitude(), results);
            if (results[0] < MAX_DIST) {
                Intent intent = new Intent(this, StoreView.class);
                intent.putExtra("store", store);
                startActivityForResult(intent, STORE_VIEW);
                return false;
            }
        }
        return true;
    }

    public void getClosestPantry(List<PantryList> list, Location location) {
        for (PantryList pantry : list) {
            float[] results = new float[1];
            Location.distanceBetween(pantry.getCoords().getLatitude(), pantry.getCoords().getLongitude(), location.getLatitude(), location.getLongitude(), results);
            if (results[0] < MAX_DIST) {
                Intent intent = new Intent(this, PantryDetails.class);
                intent.putExtra("pantryList", pantry);
                startActivityForResult(intent, VIEW_DETAILS);
            }
        }
    }

    public void gotoPantryMenu(View view) {
        Intent intent = new Intent(this, PantryMenu.class);
        startActivity(intent);
    }

    public void gotoStoreMenu(View view) {
        Intent intent = new Intent(this, StoreMenu.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // Disabled
    }

    public void logout(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(LOGOUT_KEY, true);
        startActivity(intent);
        finish();
    }

    public void setLocale(View view) {

        SharedPreferences settings = getSharedPreferences(PREF_LOCALE, Context.MODE_PRIVATE);
        String currentLocale = settings.getString(PREF_LOCALE, PREF_LOCALE_DEFAULT);
        SharedPreferences.Editor editor = settings.edit();

        Locale locale;
        if (currentLocale.equals(PREF_LOCALE_DEFAULT)) {
            locale = new Locale(PREF_LOCALE_PT);
            editor.putString(PREF_LOCALE, PREF_LOCALE_PT);
        } else {
            locale = new Locale(PREF_LOCALE_DEFAULT);
            editor.putString(PREF_LOCALE, PREF_LOCALE_DEFAULT);
        }

        editor.apply();

        Locale.setDefault(locale);
        Resources resources = this.getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
        setContentView(R.layout.activity_main_menu);
    }
}