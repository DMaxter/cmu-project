package pt.ulisboa.tecnico.cmu.shopist.pantry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

public class UserListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<String> users;
    private final PantryList pantryList;

    public UserListAdapter(Context context, List<String> values, PantryList pantryList) {
        super(context, R.layout.adapter_user_list, values);
        this.context = context;
        this.users = values;
        this.pantryList = pantryList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_user_list, parent, false);
        TextView nameView = rowView.findViewById(R.id.user_name);


        // Minus button view and onClickListener
        Button removeBtn = rowView.findViewById(R.id.remove_user_btn);
        removeBtn.setTag(users.get(position));
        removeBtn.setOnClickListener(v -> new AlertDialog.Builder(getContext())
                .setTitle("Unshare")
                .setMessage("Do you really want to unshare \"" + pantryList.getName() + "\" with " + users.get(position) + " ?")
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> new Thread(() -> {
                    try {
                        unshareWithUser(users.get(position));
                    } catch (ServerUnreachableException e) {
                        e.printStackTrace();
                    }
                }).start())
                .setNegativeButton(android.R.string.cancel, null).show());

        nameView.setText(users.get(position));

        return rowView;
    }

    public void unshareWithUser(String user) throws ServerUnreachableException {
        SharedPreferences settings = getContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean("Logged", false);
        String owner;
        if (logged) {
            owner = settings.getString("Username", "");
        }
        else {
            owner = settings.getString("GUID", "");
        }
        RemoteOperations.unshareList(owner, user, pantryList.getId().toString());
        Intent intent = new Intent("updatePantryUsers");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

}
