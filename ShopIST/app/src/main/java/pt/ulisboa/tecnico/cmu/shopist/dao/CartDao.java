package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;

@Dao
public interface CartDao {
    @Insert
    void insert(ItemInCart item);

    @Insert
    void insertAll(List<ItemInCart> items);

    @Update
    void update(ItemInCart item);

    @Update
    void updateAll(List<ItemInCart> items);

    @Delete
    void delete(ItemInCart item);

    @Delete
    void deleteAll(List<ItemInCart> items);

    @Query("SELECT * FROM cart WHERE store_name = :store AND item_name = :item")
    ItemInCart getItem(String store, String item);

    @Query("SELECT * FROM cart")
    List<ItemInCart> getAll();

    @Query("DELETE FROM cart")
    void flushCart();
}
