package pt.ulisboa.tecnico.cmu.shopist.pantry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;
import java.util.stream.Collectors;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.Hasher;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.dto.ListPantriesDto;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.AlreadyUpdatedException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.serializer.ListPantriesDtoJsonSerializer;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

public class PantryMenu extends AppCompatActivity {

    ListView listView;
    List<PantryList> list;
    String user;
    private SwipeRefreshLayout swipe;
    private final int CREATE_PANTRY = 1;
    public static final int VIEW_DETAILS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantry_menu);
        this.setTitle(R.string.pantry_lists);


        listView = findViewById(R.id.list_view);
        swipe = findViewById(R.id.swiper_refresh_pantries);

        swipe.setOnRefreshListener(this::showLists);

        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        if (logged) {
            user = settings.getString(PREF_USER, "");
        } else {
            user = settings.getString(PREF_GUID, "");
        }

        loadFromDB();
    }

    public void addPantryList(View view) {
        Intent intent = new Intent(this, CreatePantryActivity.class);
        startActivityForResult(intent, CREATE_PANTRY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_PANTRY) {
            // If a list was created
            if (resultCode == Activity.RESULT_OK) {
                PantryList pantry = (PantryList) data.getSerializableExtra(CreatePantryActivity.RESULT_PANTRY);

                list.add(pantry);
                addPantry(pantry);
                showLists();
            }
        } else if (requestCode == VIEW_DETAILS) {
            // If a list was deleted
            if (resultCode == Activity.RESULT_OK) {
                loadFromDB();
                showLists();
            }
        }
    }

    public void showLists() {
        new Thread(() -> {
            try {
                List<PantryList> newList = RemoteOperations.getPantryLists(user, Hasher.hash(ListPantriesDtoJsonSerializer.serialize(new ListPantriesDto(list))));
                deletePantries(list.stream().filter(e -> !newList.contains(e)).collect(Collectors.toList()));
                addPantries(newList.stream().filter(e -> !list.contains(e)).collect(Collectors.toList()));
                updatePantries(newList.stream().filter(e -> list.contains(e)).collect(Collectors.toList()));
                list = newList;
            } catch (InvalidListException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                );
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            } catch (AlreadyUpdatedException ignored) {
            }

            runOnUiThread(() -> {
                ArrayAdapter<PantryList> pantryListAdapter = new PantryMenuAdapter(this, list, user);
                listView.setAdapter(pantryListAdapter);

                listView.setOnItemClickListener((parent, view, position, id) -> {
                    PantryList pantryList = pantryListAdapter.getItem(position);
                    Intent intent = new Intent(this, PantryDetails.class);
                    intent.putExtra("pantryList", pantryList);
                    startActivityForResult(intent, VIEW_DETAILS);
                });
                swipe.setRefreshing(false);
            });
        }).start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, ReceivePantry.class);
        startActivity(intent);

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.qr_code_menu, menu);
        return true;
    }

    // Database methods

    private void addPantry(PantryList list) {
        new Thread(() -> Database.addPantryList(list)).start();
    }

    private void addPantries(List<PantryList> list) {
        Database.addPantryLists(list);
    }

    private void updatePantries(List<PantryList> list) {
        Database.updatePantryLists(list);
    }

    private void deletePantries(List<PantryList> list) {
        Database.deletePantryLists(list);
    }

    private void loadFromDB() {
        new Thread(() -> {
            list = Database.getPantryLists();
            runOnUiThread(this::showLists);
        }).start();
    }
}