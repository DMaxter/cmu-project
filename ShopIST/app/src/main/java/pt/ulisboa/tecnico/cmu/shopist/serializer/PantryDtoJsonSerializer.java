package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.AmountDto;
import pt.ulisboa.tecnico.cmu.shopist.dto.CoordinatesDto;
import pt.ulisboa.tecnico.cmu.shopist.dto.PantryDto;

public class PantryDtoJsonSerializer implements JsonSerializer<PantryDto> {
    @Override
    public JsonElement serialize(PantryDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("id", context.serialize(dto.getId()));
        obj.add("name", context.serialize(dto.getName()));
        obj.add("coords", context.serialize(dto.getCoords()));
        obj.add("items", context.serialize(dto.getItems()));

        return obj;
    }

    public static String serialize(PantryDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(PantryDto.class, new PantryDtoJsonSerializer())
                .registerTypeAdapter(AmountDto.class, new AmountDtoJsonSerializer())
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}