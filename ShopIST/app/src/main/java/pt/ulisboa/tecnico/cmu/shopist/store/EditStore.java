package pt.ulisboa.tecnico.cmu.shopist.store;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

public class EditStore extends AppCompatActivity {
    private CreateStoreItemAdapter CreateStoreItemAdapter;
    private ListView listView;
    private List<Item> list;
    private Store store;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_store);

        TextView nameField = findViewById(R.id.edit_store_name);
        listView = findViewById(R.id.list_view);

        store = (Store) getIntent().getSerializableExtra("storeToEdit");

        nameField.setText(store.getName());

        //Get user info
        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean(PREF_LOGGED, false);

        if (logged) {
            user = settings.getString(PREF_USER, "");
        }
        else {
            user = settings.getString(PREF_GUID, "");
        }


        //Get items filtered items from server
        new Thread(() -> {
            try {
                list = Database.getItemsNotInStore(store.getName());
                List<Item> newList = RemoteOperations.getItemsNotInStore(store.getName());
                addItems(newList.stream().filter(e -> !list.contains(e)).collect(Collectors.toList()));
                updateItems(newList.stream().filter(e -> list.contains(e)).collect(Collectors.toList()));
                list = newList;
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            }

            runOnUiThread(() -> {
                CreateStoreItemAdapter = new CreateStoreItemAdapter(this, list);
                listView.setAdapter(CreateStoreItemAdapter);
            });
        }).start();
    }



    public void editStore(View view) {

        HashMap<String,Double> checkItems = CreateStoreItemAdapter.getCheckedItems();
        new Thread(() -> {
            try {
                String storeName = store.getName();
                RemoteOperations.addItemsToStore(storeName, checkItems);
                checkItems.forEach((k ,v) -> addItemInStore(storeName, k, v));
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            }

            setResult(Activity.RESULT_OK, new Intent());
            finish();
        }).start();
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    private void addItems(List<Item> list) {
        Database.addItems(list);
    }

    private void updateItems(List<Item> list) {
        Database.updateItems(list);
    }

    private void addItemInStore(String store, String item, double price) {
        Database.addItemToStore(store, item, price);
    }
}