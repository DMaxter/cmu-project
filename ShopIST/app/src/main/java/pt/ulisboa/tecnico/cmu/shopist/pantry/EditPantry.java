package pt.ulisboa.tecnico.cmu.shopist.pantry;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

public class EditPantry extends AppCompatActivity implements TextWatcher {
    private Coordinates coords;
    private ImageView pantryImage;
    private EditText nameField;
    private TextView nameNotSelected;
    private final int LAUNCH_MAP = 1;
    private Boolean error = false;
    private PantryList pantryList;
    public static final String PANTRY_COORDS = "pantryCoords";
    public static final String NEW_PANTRY = "editedPantry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pantry);

        pantryList = (PantryList) getIntent().getSerializableExtra("pantryToEdit");
        coords = pantryList.getCoords();

        pantryImage =  findViewById(R.id.edit_pantry_image);
        getGoogleMapThumbnail(pantryList.getCoords());



        nameField = findViewById(R.id.edit_pantry_name);
        nameField.setText(pantryList.getName());
        nameField.addTextChangedListener(this);
        nameNotSelected = findViewById(R.id.name_not_selected);


    }

    public  void getGoogleMapThumbnail(Coordinates location){


        new Thread(() -> {
            try {
                URL url = new URL("https://maps.googleapis.com/maps/api/staticmap?center=" +location.getLatitude() + "," + location.getLongitude() + "&markers=color:red%7C"+ location.getLatitude() +","+ location.getLongitude() +"&zoom=15&size=250x250&sensor=false&key=AIzaSyAy6vSql-qRt9yvqBzejSWMl5hMfR5pxdk");
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                final Bitmap bmp = BitmapFactory.decodeStream(in);
                runOnUiThread(() -> pantryImage.setImageBitmap(bmp));
                in.close();
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void selectLocation(View view) {
        Intent intent = new Intent(getApplicationContext(), PantryMapCreation.class);
        intent.putExtra(PANTRY_COORDS, coords);
        startActivityForResult(intent, LAUNCH_MAP);
    }

    public void editList(View view) {

        if (this.nameField.getText().toString().equals("")) {
            // Display error if no name provided
            error = true;
            nameNotSelected.setVisibility(View.VISIBLE);
            nameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
        } else {
            new Thread(() -> {
                SharedPreferences settings = getSharedPreferences("Login", Context.MODE_PRIVATE);
                boolean logged = settings.getBoolean("Logged", false);
                String user;
                if (logged) {
                    user = settings.getString("Username", "");
                }
                else {
                    user = settings.getString("GUID", "");
                }

                try {
                    RemoteOperations.editPantryList(user,pantryList.getId().toString(),nameField.getText().toString(), coords.getLatitude(), coords.getLongitude());
                    pantryList.setName(nameField.getText().toString());
                    pantryList.setLocation(new Coordinates(coords.getLatitude(), coords.getLongitude()));
                    Intent intent = new Intent();
                    intent.putExtra(NEW_PANTRY, pantryList);
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                } catch (InvalidListException | InvalidCoordinatesException e) {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again.", Toast.LENGTH_LONG).show());
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show());
                }
            }).start();


        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_MAP) {
            if(resultCode == Activity.RESULT_OK){
                coords = (Coordinates) data.getSerializableExtra("coords");
                getGoogleMapThumbnail(coords);

            }
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (error) {
            error = false;
            nameNotSelected.setVisibility(View.INVISIBLE);
            nameField.getBackground().mutate().setColorFilter(null);
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}