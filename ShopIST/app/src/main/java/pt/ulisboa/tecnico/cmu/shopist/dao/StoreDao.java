package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

@Dao
public interface StoreDao {
    @Insert
    void insert(Store store);

    @Insert
    void insertAll(List<Store> list);

    @Update
    void update(Store store);

    @Update
    void updateAll(List<Store> list);

    @Query("SELECT * FROM store")
    List<Store> getAll();

    @Query("INSERT INTO store_item (store_name, item_name, price) VALUES (:store, :item, :price)")
    void insertItem(String store, String item, double price);

    @Query("UPDATE store_item SET price = :price WHERE store_name = :store AND item_name = :item")
    void updateItem(String store, String item, double price);

    @Query("DELETE FROM store_item WHERE store_name = :store AND item_name = :item")
    void deleteItem(String store, String item);

    @Query("SELECT * FROM store s INNER JOIN store_item JOIN item i WHERE s.store_name = :store AND i.item_name = :item")
    ItemInStore getItem(String store, String item);

    @Query("SELECT * FROM store s INNER JOIN store_item JOIN item WHERE s.store_name = :store")
    List<ItemInStore> getItems(String store);

    @Query("SELECT * FROM item WHERE item_name NOT IN (SELECT item_name FROM store_item WHERE store_name = :store)")
    List<Item> getItemsNotInStore(String store);

    @Query("SELECT SUM(amount_shop) AS amount, i.item_name, price, s.store_name, s.coordinates, barcode FROM store s INNER JOIN store_item JOIN pantry_item JOIN item i WHERE s.store_name = :store GROUP BY i.item_name")
    List<ItemToBuy> getItemsToBuy(String store);
}
