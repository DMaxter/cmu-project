package pt.ulisboa.tecnico.cmu.shopist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.UUID;

import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidLoginException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.store.QueueCommunicator;

public class MainActivity extends AppCompatActivity implements TextWatcher {
    private EditText usernameField;
    private EditText passwordField;
    private Button loginButton;
    private ProgressBar loadingLogin;
    private Button signUpButton;
    private Button guestButton;
    private TextView wrongCredentials;
    private Button localeButton;
    private boolean error = false;
    private String GUID;
    private boolean logged = PREF_LOGGED_DEFAULT;
    private boolean override = false;

    // Values for prefs
    public static final String PREF_NAME = "Login";
    public static final String PREF_USER = "Username";
    public static final String PREF_USER_DEFAULT = "";
    public static final String PREF_PASS = "Password";
    public static final String PREF_PASS_DEFAULT = "";
    public static final String PREF_GUID = "GUID";
    public static final String PREF_GUID_DEFAULT = "";
    public static final String PREF_LOGGED = "Logged";
    public static final boolean PREF_LOGGED_DEFAULT = false;
    public static final String PREF_LOCALE = "LOCALE";
    public static final String PREF_LOCALE_DEFAULT = "en";
    public static final String PREF_LOCALE_PT = "pt";

    // Value of register request
    public static final int REGISTER_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check if user clicked on logout
        Intent intent = getIntent();
        override = intent.getBooleanExtra(MainMenu.LOGOUT_KEY, false);

        // Get UI elements
        usernameField = findViewById(R.id.login_username_field);
        passwordField = findViewById(R.id.login_password_field);
        loginButton = findViewById(R.id.login_btn);
        loadingLogin = findViewById(R.id.login_loading);
        signUpButton = findViewById(R.id.signup_btn);
        guestButton = findViewById(R.id.guest);
        localeButton = findViewById(R.id.changeLocale);
        wrongCredentials = findViewById(R.id.wrong_credentials);

        // Set text watcher
        usernameField.addTextChangedListener(this);
        passwordField.addTextChangedListener(this);

        QueueCommunicator.init(getApplicationContext(), getMainLooper());
        RemoteOperations.init();
        Database.init(getApplicationContext());
    }

    public void guestLogin(View view) {
        if (GUID.equals("")) {
            GUID = UUID.randomUUID().toString();
        }

        Thread t = new Thread(() -> {
            try {
                RemoteOperations.guestLogin(GUID, hash(GUID));
            } catch (InvalidLoginException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Invalid login", Toast.LENGTH_LONG).show()
                );
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_SHORT).show()
                );
            }
        });

        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, MainMenu.class);
        startActivity(intent);
        finish();

    }

    public void signUp(View view) {
        Intent intent = new Intent(this, SignUp.class);
        intent.putExtra(SignUp.INTENT_GUID, GUID);
        startActivityForResult(intent, REGISTER_CODE);
    }

    public void login(View view) {
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        if (!username.equals("") && !password.equals("")) {
            lockUI();

            Thread t = new Thread(() -> {
                try {
                    RemoteOperations.login(username, hash(password));
                    runOnUiThread(() -> {
                        logged = true;
                        Intent intent = new Intent(this, MainMenu.class);
                        startActivity(intent);
                        finish();
                    });
                } catch (InvalidLoginException e) {
                    runOnUiThread(() -> {
                        unlockUI();
                        logged = false;
                        usernameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                        passwordField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                        wrongCredentials.setVisibility(View.VISIBLE);
                    });
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() -> {
                        unlockUI();
                        logged = false;
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_SHORT).show();
                    });

                }
            });
            t.start();

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        } else {
            wrongCredentials.setVisibility(View.VISIBLE);
            usernameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
            passwordField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
        }
    }

    public void lockUI() {
        loginButton.setVisibility(View.GONE);
        loadingLogin.setVisibility(View.VISIBLE);
        signUpButton.setEnabled(false);
        guestButton.setEnabled(false);
        usernameField.setEnabled(false);
        passwordField.setEnabled(false);
        localeButton.setEnabled(false);

    }

    public void unlockUI() {
        loginButton.setVisibility(View.VISIBLE);
        loadingLogin.setVisibility(View.INVISIBLE);
        signUpButton.setEnabled(true);
        guestButton.setEnabled(true);
        usernameField.setEnabled(true);
        passwordField.setEnabled(true);
        localeButton.setEnabled(true);
    }

    private void loadPreferences() {
        SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        usernameField.setText(settings.getString(PREF_USER, PREF_USER_DEFAULT));
        passwordField.setText(settings.getString(PREF_PASS, PREF_PASS_DEFAULT));
        GUID = settings.getString(PREF_GUID, PREF_GUID_DEFAULT);
        logged = settings.getBoolean(PREF_LOGGED, PREF_LOGGED_DEFAULT);
    }

    private void savePreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(PREF_USER, usernameField.getText().toString());
        editor.putString(PREF_PASS, passwordField.getText().toString());
        editor.putString(PREF_GUID, GUID);
        editor.putBoolean(PREF_LOGGED, logged);
        editor.apply();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void onResume() {
        super.onResume();
        loadPreferences();

        if (override) {
            logged = false;
        }else if (logged) {
            loginButton.performClick();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        savePreferences();
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (error) {
            error = false;
            wrongCredentials.setVisibility(View.INVISIBLE);
            usernameField.getBackground().mutate().setColorFilter(null);
            passwordField.getBackground().mutate().setColorFilter(null);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REGISTER_CODE) {
            if (resultCode == SignUp.RESULT_OK) {
                usernameField.setText(data.getStringExtra(SignUp.RESULT_USER_KEY));
                passwordField.setText(data.getStringExtra(SignUp.RESULT_PASS_KEY));
                GUID = data.getStringExtra(SignUp.RESULT_GUID);

                savePreferences();
                // Login
                loginButton.performClick();
            }
        }
    }

    public void setLocale(View view) {

        SharedPreferences settings = getSharedPreferences(PREF_LOCALE, Context.MODE_PRIVATE);
        String currentLocale = settings.getString(PREF_LOCALE, PREF_LOCALE_DEFAULT);
        SharedPreferences.Editor editor = settings.edit();


        Locale locale;
        if (currentLocale.equals(PREF_LOCALE_DEFAULT)) {
            locale = new Locale(PREF_LOCALE_PT);
            editor.putString(PREF_LOCALE, PREF_LOCALE_PT);
        } else {
            locale = new Locale(PREF_LOCALE_DEFAULT);
            editor.putString(PREF_LOCALE, PREF_LOCALE_DEFAULT);
        }

        editor.apply();

        Locale.setDefault(locale);
        Resources resources = this.getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
        setContentView(R.layout.activity_main);
    }

    public static String hash(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] digest = md.digest(text.getBytes());
            StringBuilder hex = new StringBuilder();
            for (byte b : digest) {
                hex.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return hex.toString();
        } catch (NoSuchAlgorithmException e) {
            // Ignore
            return "";
        }
    }
}