package pt.ulisboa.tecnico.cmu.shopist.store;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.MainMenu;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

public class StoreMenu extends AppCompatActivity {

    ListView listView;
    List<Store> list;
    String user;
    private final int CREATE_STORE = 1;
    public static final int STORE_VIEW = 2;
    private Location location;
    private FusedLocationProviderClient fusedLocationClient;
    private SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_menu);
        this.setTitle(R.string.store_lists);

        new Thread(() -> {
            list = Database.getStores();
            showStores(null);


            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

            } else {
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, location -> {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                showStores(location);
                            }
                        });
            }
        }).start();

        listView = findViewById(R.id.list_view);
        swipe = findViewById(R.id.swiper_refresh_stores);
        swipe.setOnRefreshListener(() -> {
            // Got last known location. In some rare situations this can be null.
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, this::showStores);
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            this.location = location;
                            showStores(location);
                        }
                    });
        }
    }

    public void addStore(View view) {
        Intent intent = new Intent(this, CreateStoreActivity.class);
        startActivityForResult(intent, CREATE_STORE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_STORE) {
            // If a store was created
            if (resultCode == Activity.RESULT_OK) {
                Store store = (Store) data.getSerializableExtra(CreateStoreActivity.RESULT_STORE);
                Map<String, Double> items = (Map<String, Double>) data.getSerializableExtra(CreateStoreActivity.RESULT_ITEMS);
                list.add(store);

                // Add store and prices to DB
                new Thread(() -> {
                    Database.addStore(store);
                    items.entrySet().forEach(e -> Database.addItemToStore(store.getName(), e.getKey(), e.getValue()));
                }).start();


                showStores(null);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

                } else {
                    fusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, location -> {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    showStores(location);
                                }
                            });
                }
            }
        }
    }


    public void showStores(Location loc) {

        try {
            List<Store> newList = RemoteOperations.getStores(MainMenu.user);
            new Thread(() -> {
                addStores(newList.stream().filter(e -> !list.contains(e)).collect(Collectors.toList()));
                updateStores(newList.stream().filter(e -> list.contains(e)).collect(Collectors.toList()));
                list = newList;
            }).start();

        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        } catch (InvalidCoordinatesException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
            );
        }

        runOnUiThread(() -> {
            ArrayAdapter<Store> storeListAdapter = new StoreMenuAdapter(this, list, loc);
            listView.setAdapter(storeListAdapter);


            listView.setOnItemClickListener((parent, view, position, id) -> {
                Store store = storeListAdapter.getItem(position);
                Intent intent = new Intent(this, StoreView.class);
                intent.putExtra("store", store);
                startActivityForResult(intent, STORE_VIEW);
            });
            swipe.setRefreshing(false);
        });
    }

    private void addStores(List<Store> list) {
        Database.addStores(list);
    }

    private void updateStores(List<Store> list) {
        Database.updateStores(list);
    }
}