package pt.ulisboa.tecnico.cmu.shopist.pantry;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.Hasher;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

import pt.ulisboa.tecnico.cmu.shopist.dto.ListSharedDto;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.AlreadyUpdatedException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.serializer.ListSharedDtoJsonSerializer;

import static android.location.Location.FORMAT_DEGREES;

public class UnsharePantry extends AppCompatActivity {
    private TextView pantryNameView;
    private TextView pantryCoordsView;
    private ListView listView;
    private PantryList pantryList;
    private String owner;

    public int SHARE_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unshare_pantry);
        pantryList = (PantryList) getIntent().getSerializableExtra("pantryToUnshare");
        listView = findViewById(R.id.list_view);

        pantryNameView = findViewById(R.id.pantry_list_name);
        pantryCoordsView = findViewById(R.id.pantry_list_coordinates);
        LocalBroadcastManager.getInstance(this).registerReceiver(updateReceiver, new IntentFilter("updatePantryUsers"));

        showPantryDetails(pantryList);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void sharePantry(View view) {
        Intent intentShare = new Intent(this, SharePantry.class);
        intentShare.putExtra("pantryToShare", pantryList);
        startActivityForResult(intentShare,SHARE_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SHARE_CODE) {
                getUsers();
            }
        }
    }

    public void showPantryDetails(PantryList pantryList) {
        String text = "Unshare: " + pantryList.getName();
        pantryNameView.setText(text);
        String lat = Location.convert(pantryList.getCoords().getLatitude(), FORMAT_DEGREES);
        String lon = Location.convert(pantryList.getCoords().getLongitude(), FORMAT_DEGREES);
        String loc = lat + ", " + lon;
        pantryCoordsView.setText(loc);

        SharedPreferences settings = getSharedPreferences("Login", Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean("Logged", false);
        if (logged) {
            owner = settings.getString("Username", "");
        } else {
            owner = settings.getString("GUID", "");
        }

        getUsers();

    }

    public BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getUsers();
        }
    };

    public void getUsers() {
        try {
            List<String> user_list = new ArrayList<>();
            user_list = RemoteOperations.getUsersFromPantry(pantryList.getId().toString(), owner, Hasher.hash(ListSharedDtoJsonSerializer.serialize(new ListSharedDto(new HashSet<>(user_list)))));

            ArrayAdapter<String> userListAdapter = new UserListAdapter(this, user_list, pantryList);
            listView.setAdapter(userListAdapter);
        } catch (ServerUnreachableException e) {
            runOnUiThread(() ->
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
            );
        } catch (AlreadyUpdatedException ignored) {
        }
    }


}