package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.ListPantriesDto;

public class ListPantriesDtoJsonSerializer implements JsonSerializer<ListPantriesDto> {
        @Override
        public JsonElement serialize(ListPantriesDto dto, Type type, JsonSerializationContext context) {
            JsonObject obj = new JsonObject();

            obj.add("pantries", context.serialize(dto.getPantries()));

            return obj;
        }

        public static String serialize(ListPantriesDto o) {
            return new GsonBuilder()
                    .registerTypeAdapter(ListPantriesDto.class, new ListPantriesDtoJsonSerializer())
                    .create()
                    .toJson(o);
        }

}
