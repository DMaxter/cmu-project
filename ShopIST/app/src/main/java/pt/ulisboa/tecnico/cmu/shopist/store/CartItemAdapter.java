package pt.ulisboa.tecnico.cmu.shopist.store;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.domain.Cart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;

public class CartItemAdapter extends ArrayAdapter<ItemInCart> {
    private final Context context;
    private final List<ItemInCart> values;
    private Cart cart;


    public CartItemAdapter(Context context, Cart cart, List<ItemInCart> values) {
        super(context, R.layout.adapter_cart_items, values);
        this.context = context;
        this.cart = cart;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_cart_items, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView barcodeView = rowView.findViewById(R.id.secondLine);
        ImageView imageView = rowView.findViewById(R.id.icon);
        TextView itemAmount = rowView.findViewById(R.id.item_ammount);

        Button removeOneBtn = rowView.findViewById(R.id.remove_one);
        Button removeAllBtn = rowView.findViewById(R.id.remove_all);

        // Add one button view and onClickListener
        if (values.get(position).getAmount() == 0) {
            removeOneBtn.setEnabled(false);
        }
        removeOneBtn.setTag(values.get(position));
        removeOneBtn.setOnClickListener(v -> {
            ItemInCart item = (ItemInCart) v.getTag();
            cart.updateItemAmount(item, -1);
            int amount = item.getAmount();

            // Disable
            if (amount == 0) {
                removeAllBtn.setEnabled(false);
                removeOneBtn.setEnabled(false);
                cart.removeItem(item);
            }

            itemAmount.setText(String.valueOf(amount));

            Intent intent = new Intent("updateCartTotal");
            intent.putExtra("newTotal", cart.getTotalCost());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        });

        // Add one button view and onClickListener
        if (values.get(position).getAmount() == 0) {
            removeAllBtn.setEnabled(false);
        }
        removeAllBtn.setTag(values.get(position));
        removeAllBtn.setOnClickListener(v -> {
            ItemInCart item = (ItemInCart) v.getTag();
            removeAllBtn.setEnabled(false);
            removeOneBtn.setEnabled(false);
            cart.removeItem(item);

            itemAmount.setText(String.valueOf(0));
            Intent intent = new Intent("updateCartTotal");
            intent.putExtra("newTotal", cart.getTotalCost());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        });

        nameView.setText(values.get(position).getItem().getName());
        barcodeView.setText(String.valueOf(values.get(position).getItem().getBarcode()));
        itemAmount.setText(String.valueOf(values.get(position).getAmount()));

        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        return rowView;
    }

    public ItemInCart getItem(int position) {
        return values.get(position);
    }
}
