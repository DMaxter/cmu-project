package pt.ulisboa.tecnico.cmu.shopist.domain;

import androidx.room.ColumnInfo;

public class AmountInfo {
    @ColumnInfo(name = "amount_pantry")
    private int pantry;

    @ColumnInfo(name = "amount_shop")
    private int shop;

    public AmountInfo(int pantry, int shop) {
        this.pantry = pantry;
        this.shop = shop;
    }

    public int getAmountPantry() {
        return this.pantry;
    }

    public int getAmountShop() {
        return this.shop;
    }

    public void setAmountPantry(int amountPantry) {
        this.pantry = amountPantry;
    }

    public void setAmountShop(int amountShop) {
        this.shop = amountShop;
    }
}
