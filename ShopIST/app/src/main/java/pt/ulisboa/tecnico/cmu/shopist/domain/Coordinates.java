package pt.ulisboa.tecnico.cmu.shopist.domain;

import java.io.Serializable;

import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;

public class Coordinates implements Serializable {
    private final double latitude;
    private final double longitude;

    public Coordinates(double latitude, double longitude) throws InvalidCoordinatesException {
        if (latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180) {
            throw new InvalidCoordinatesException();
        }

        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    @Override
    public String toString() {
        return "(" + this.latitude + ", " + this.longitude + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Coordinates)) {
            return false;
        }

        return ((Coordinates) o).latitude == this.latitude && ((Coordinates) o).longitude == this.longitude;
    }
}
