package pt.ulisboa.tecnico.cmu.shopist.dto;

import java.util.List;
import java.util.TreeMap;

import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;

public class ListPantriesDto {
    private final TreeMap<String, String> pantries;

    public ListPantriesDto(List<PantryList> list) {
        this.pantries = new TreeMap<>();
        for(PantryList pantry: list) {
            this.pantries.put(pantry.getId().toString(), pantry.getName());
        }
    }

    public TreeMap<String, String> getPantries() {
        return this.pantries;
    }
}