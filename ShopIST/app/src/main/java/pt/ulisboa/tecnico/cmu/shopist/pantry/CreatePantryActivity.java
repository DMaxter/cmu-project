package pt.ulisboa.tecnico.cmu.shopist.pantry;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;
import java.util.function.Consumer;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

import static android.location.Location.FORMAT_DEGREES;

public class CreatePantryActivity extends AppCompatActivity implements TextWatcher {
    private Coordinates coords;
    private TextView locationNotSelected;
    private Button locationButton;
    private Button createPantry;
    private ProgressBar locationLoading;
    private LocationManager locationManager;
    private Consumer<Location> consumer;
    private TextView coordsText;
    private EditText nameField;
    private TextView nameNotSelected;
    private final int LAUNCH_MAP = 1;
    private Boolean error = false;

    public static final String MY_COORDS = "myCoords";
    public static final String PANTRY_COORDS = "pantryCoords";

    public static final String RESULT_PANTRY = "ResultPantry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pantry);

        locationNotSelected = findViewById(R.id.location_not_selected);
        locationButton = findViewById(R.id.select_location_btn);
        locationLoading = findViewById(R.id.locating_loading);
        coordsText = findViewById(R.id.coords_txt);
        nameField = findViewById(R.id.create_pantry_name);
        nameField.addTextChangedListener(this);
        nameNotSelected = findViewById(R.id.name_not_selected);
        createPantry = findViewById(R.id.create_pantry_btn);


        consumer = location -> {
            Intent intent = new Intent(getApplicationContext(), PantryMapCreation.class);
            try {
                intent.putExtra(MY_COORDS,new Coordinates(location.getLatitude(),location.getLongitude()));
                if (coords!=null) {
                    // If previously defined, add Coordinates so they can be edited
                    intent.putExtra(PANTRY_COORDS, coords);
                }
            } catch (InvalidCoordinatesException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                );
            }
            startActivityForResult(intent, LAUNCH_MAP);
        };
    }

    public void selectLocation(View view) {
        lockUI();
        locateUser();
    }

    public void lockUI() {
        locationButton.setVisibility(View.GONE);
        locationLoading.setVisibility(View.VISIBLE);
        createPantry.setEnabled(false);

    }

    public void unlockUI() {
        locationButton.setVisibility(View.VISIBLE);
        locationLoading.setVisibility(View.INVISIBLE);
        createPantry.setEnabled(true);
    }

    public void createList(View view) {

        if (this.nameField.getText().toString().equals("")) {
            // Display error if no name provided
            error = true;
            nameNotSelected.setVisibility(View.VISIBLE);
            nameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
        } else if (this.coords == null) {
            // Display error if location not selected
            locationNotSelected.setVisibility(View.VISIBLE);
        } else {
            String name = this.nameField.getText().toString();
            double latitude = this.coords.getLatitude();
            double longitude = this.coords.getLongitude();
            UUID id = UUID.randomUUID();

            SharedPreferences settings = getSharedPreferences("Login", Context.MODE_PRIVATE);
            boolean logged = settings.getBoolean("Logged", false);
            String user;
            if (logged) {
                user = settings.getString("Username", "");
            } else {
                user = settings.getString("GUID", "");
            }

            new Thread(() -> {
                try {
                    RemoteOperations.createPantryList(user, id.toString(), name, latitude, longitude);
                } catch (InvalidListException e) {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again.", Toast.LENGTH_LONG).show());
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show());
                }
            }).start();

            Intent intent = new Intent();
            try {
                intent.putExtra(RESULT_PANTRY, new PantryList(id, new Coordinates(latitude, longitude), name, user));
            } catch (InvalidCoordinatesException e) {
                // Won't occur
            }

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @SuppressLint("MissingPermission")
    public void locateUser() {
        checkPermissions();
        locationManager.getCurrentLocation(LocationManager.GPS_PROVIDER,null,getMainExecutor(),consumer);
    }

    public void checkPermissions() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // If Location is disabled, prompt user to turn it ON
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionsIntent);
        }

        // If user hasn't granted Location Permissions, prompt for permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_MAP) {
            if(resultCode == Activity.RESULT_OK){
                coords = (Coordinates) data.getSerializableExtra("coords");
                String lat = Location.convert(coords.getLatitude(), FORMAT_DEGREES);
                String lon = Location.convert(coords.getLongitude(), FORMAT_DEGREES);
                String loc = lat + ", " + lon;
                coordsText.setText(loc);
                locationNotSelected.setVisibility(View.INVISIBLE);

            }
            unlockUI();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (error) {
            error = false;
            nameNotSelected.setVisibility(View.INVISIBLE);
            nameField.getBackground().mutate().setColorFilter(null);
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}