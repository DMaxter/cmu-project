package pt.ulisboa.tecnico.cmu.shopist.relations;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

import java.io.Serializable;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;

public class ItemInStore implements Serializable {
    @Embedded
    private Store store;

    @Embedded
    private Item item;

    @ColumnInfo(name = "price")
    private double price;

    public ItemInStore(Store store, Item item, double price) {
        this.store = store;
        this.item = item;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Store getStore() {
        return store;
    }

    public Item getItem() {
        return item;
    }

    public String getName() {
        return item.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemInStore that = (ItemInStore) o;
        return store.equals(that.store) &&
                item.equals(that.item);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
