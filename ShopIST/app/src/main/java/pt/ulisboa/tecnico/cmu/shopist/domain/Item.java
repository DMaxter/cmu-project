package pt.ulisboa.tecnico.cmu.shopist.domain;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidImageException;

@Entity(tableName = "item")
public class Item implements Serializable {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "item_name")
    private String name;

    @ColumnInfo(name = "pictures")
    private List<String> pictures = new ArrayList<>();

    @ColumnInfo(name = "barcode")
    private String barcode;

    @Ignore
    public Item(String name) {
        this(name, "");
    }

    public Item(String name, String barcode) {
        this.name = name;
        this.barcode = barcode;
    }

    public void addPicture(String picture) throws InvalidImageException {
        if (picture == null) {
            throw new InvalidImageException();
        }

        this.pictures.add(picture);
    }

    public String getName() {
        return name;
    }


    public List<String> getPictures() {
        return pictures;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item that = (Item) o;
        return name.equals(that.name);
    }
}
