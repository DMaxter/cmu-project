package pt.ulisboa.tecnico.cmu.shopist.domain;

import java.io.Serializable;
import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

public class Cart implements Serializable {
    private Store store;
    private double totalCost = 0;
    private List<ItemInCart> items;

    public Cart(Store store) {
        this.store = store;
        new Thread(() -> this.items = Database.getCart()).start();
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public void updateItemAmount(ItemToBuy item, int amount) {
        for (ItemInCart i : items) {
            if (i.getName().equals(item.getName())) {
                if (i.getItemToBuy() == null) {
                    i.setItemToBuy(item);
                }

                i.addAmount(amount);
                new Thread(() -> Database.updateItemInCart(i)).start();
                totalCost += amount * item.getPrice();
                return;
            }
        }

        ItemInCart cartItem = new ItemInCart(this.store, item, amount);
        items.add(cartItem);
        item.addAmount(-amount);
        new Thread(() -> Database.addItemToCart(cartItem)).start();
        totalCost += amount * item.getPrice();
    }

    public void updateItemAmount(ItemInCart item, int amount) {
        updateItemAmount(item.getItemToBuy(), amount);
    }

    public List<ItemInCart> getItems() {
        return this.items;
    }

    public void removeItem(ItemInCart i) {
        for (int k = 0; k < items.size(); k++) {
            if (items.get(k).getName().equals(i.getItem().getName())) {
                ItemInCart cartItem = items.remove(k);
                totalCost -= cartItem.getAmount() * cartItem.getPrice();
                new Thread(() -> {
                    Database.deleteItemFromCart(i);
                    cartItem.remove();
                }).start();

                return;
            }
        }
    }

    public List<ItemInCart> getItemsInCart() {
        return items;
    }

    public void update(List<ItemInCart> items) {
        totalCost = items.stream()
                .mapToDouble(e -> e.getAmount() * e.getPrice())
                .sum();
        new Thread(() -> {
            Database.flushCart();
            Database.addItemsToCart(items);
        }).start();
    }

    public void clean() {
        items.clear();
        totalCost = 0;
        new Thread(Database::flushCart).start();
    }
}
