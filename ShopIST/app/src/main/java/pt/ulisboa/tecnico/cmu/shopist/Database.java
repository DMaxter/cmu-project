package pt.ulisboa.tecnico.cmu.shopist;

import android.content.Context;

import androidx.room.Room;

import java.util.List;
import java.util.UUID;

import pt.ulisboa.tecnico.cmu.shopist.dao.ShopISTDatabase;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

public class Database {
    private static ShopISTDatabase db;

    public static void init(Context context) {
        db = Room.databaseBuilder(context, ShopISTDatabase.class, "shopist").build();
    }


    // Pantries
    public static void addPantryList(PantryList pantry) {
        db.pantryListDao().insert(pantry);
    }

    public static void updatePantryList(PantryList pantry) {
        db.pantryListDao().update(pantry);
    }

    public static void deletePantryList(PantryList pantry) {
        db.pantryListDao().delete(pantry);
    }

    public static void addPantryLists(List<PantryList> list) {
        db.pantryListDao().insertAll(list);
    }

    public static void updatePantryLists(List<PantryList> list) {
        db.pantryListDao().updateAll(list);
    }

    public static void deletePantryLists(List<PantryList> list) {
        db.pantryListDao().deleteAll(list);
    }

    public static List<PantryList> getPantryLists() {
        return db.pantryListDao().getAll();
    }

    public static void registerOwner(String GUID, String username) {
        db.pantryListDao().registerOwner(GUID, username);
    }

    // Items
    public static boolean hasItem(String item) {
        return db.itemDao().getItem(item) != null;
    }

    public static Item getItem(String item) {
        return db.itemDao().getItem(item);
    }

    public static void updateItemAmount(UUID pantryId, String item, int shop, int pantry) {
        db.pantryListDao().updateItemAmounts(pantryId, item, shop, pantry);
    }

    public static void deleteItemFromPantry(UUID pantryId, String name) {
        db.pantryListDao().deleteItem(pantryId, name);
    }

    public static void addItem(Item item) {
        db.itemDao().insert(item);
    }

    public static void updateItem(Item item) {
        db.itemDao().update(item);
    }

    public static void addItems(List<Item> list) {
        db.itemDao().insertAll(list);
    }

    public static void updateItems(List<Item> items) {
        db.itemDao().updateAll(items);
    }

    public static List<ItemInPantryList> getItemsInPantry(UUID pantryId) {
        return db.pantryListDao().getItems(pantryId);
    }

    public static void addItemToStore(String store, String item, double price) {
        db.storeDao().insertItem(store, item, price);
    }

    public static void updateStoreItem(String store, String item, double price) {
        db.storeDao().updateItem(store, item, price);
    }

    public static void deleteItemFromStore(String store, String item) {
        db.storeDao().deleteItem(store, item);
    }

    public static List<ItemToBuy> getItemsToBuy(String store) {
        return db.storeDao().getItemsToBuy(store);
    }

    public static ItemInStore getItemInStore(String store, String item) {
        return db.storeDao().getItem(store, item);
    }

    public static List<Item> getItemsNotInStore(String store) {
        return db.storeDao().getItemsNotInStore(store);
    }

    public static List<Item> getAllItems() {
        return db.itemDao().getAll();
    }


    // Stores
    public static void addStore(Store store) {
        db.storeDao().insert(store);
    }

    public static void updateStore(Store store) {
        db.storeDao().update(store);
    }

    public static void addStores(List<Store> list) {
        db.storeDao().insertAll(list);
    }

    public static void updateStores(List<Store> list) {
        db.storeDao().updateAll(list);
    }

    public static List<Store> getStores() {
        return db.storeDao().getAll();
    }


    // Cart
    public static void addItemToCart(ItemInCart item) {
        db.cartDao().insert(item);
    }

    public static void updateItemInCart(ItemInCart item) {
        db.cartDao().update(item);
    }

    public static void deleteItemFromCart(ItemInCart item) {
        db.cartDao().delete(item);
    }

    public static void addItemsToCart(List<ItemInCart> items) {
        db.cartDao().insertAll(items);
    }

    public static void updateItemsInCart(List<ItemInCart> items) {
        db.cartDao().updateAll(items);
    }

    public static void deleteItemsFromCart(List<ItemInCart> items) {
        db.cartDao().deleteAll(items);
    }

    public static ItemInCart getItemFromCart(String store, String item) {
        return db.cartDao().getItem(store, item);
    }

    public static List<ItemInCart> getCart() {
        return db.cartDao().getAll();
    }

    public static void flushCart() {
        db.cartDao().flushCart();
    }
}
