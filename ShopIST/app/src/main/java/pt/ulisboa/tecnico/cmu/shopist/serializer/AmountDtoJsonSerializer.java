package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.AmountDto;

public class AmountDtoJsonSerializer implements JsonSerializer<AmountDto> {
    @Override
    public JsonElement serialize(AmountDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("pantry", context.serialize(dto.getAmountPantry()));
        obj.add("shop", context.serialize(dto.getAmountShop()));

        return obj;
    }

    public static String serialize(AmountDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(AmountDto.class, new AmountDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
