package pt.ulisboa.tecnico.cmu.shopist.dto;

import java.util.Collection;
import java.util.TreeMap;

import pt.ulisboa.tecnico.cmu.shopist.domain.Store;

public class ListStoresDto {
    private final TreeMap<String, CoordinatesDto> shops;

    public ListStoresDto(Collection<Store> stores) {
        this.shops = new TreeMap<>();

        for (Store s: stores) {
            this.shops.put(s.getName(), new CoordinatesDto(s.getCoords()));
        }
    }

    public TreeMap<String, CoordinatesDto> getShops() {
        return shops;
    }
}

