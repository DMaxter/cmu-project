package pt.ulisboa.tecnico.cmu.shopist.store;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidCoordinatesException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;

import static android.location.Location.FORMAT_DEGREES;

public class CreateStoreActivity extends AppCompatActivity implements TextWatcher {
    private Coordinates coords;
    private TextView locationNotSelected;
    private Button locationButton;
    private Button createStore;
    private ProgressBar locationLoading;
    private LocationManager locationManager;
    private Consumer<Location> consumer;
    private TextView coordsText;
    private EditText nameField;
    private TextView nameNotSelected;
    private CreateStoreItemAdapter CreateStoreItemAdapter;
    private ListView listView;
    private List<Item> list;
    private final int LAUNCH_MAP = 1;
    private Boolean error = false;

    public static final String MY_COORDS = "myCoords";
    public static final String STORE_COORDS = "storeCoords";

    public static final String RESULT_STORE = "StoreResult";
    public static final String RESULT_ITEMS = "ItemsResult";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_store);

        locationNotSelected = findViewById(R.id.location_not_selected);
        locationButton = findViewById(R.id.select_location_btn);
        locationLoading = findViewById(R.id.locating_loading);
        coordsText = findViewById(R.id.coords_txt);
        nameField = findViewById(R.id.create_store_name);
        nameField.addTextChangedListener(this);
        nameNotSelected = findViewById(R.id.name_not_selected);
        createStore = findViewById(R.id.create_store_btn);
        listView = findViewById(R.id.list_view);


        consumer = location -> {
            Intent intent = new Intent(getApplicationContext(), StoreMapCreation.class);
            try {
                intent.putExtra(MY_COORDS, new Coordinates(location.getLatitude(),location.getLongitude()));
                if (coords!=null) {
                    // If previously defined, add Coordinates so they can be edited
                    intent.putExtra(STORE_COORDS, coords);
                }
            } catch (InvalidCoordinatesException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_LONG).show()
                );
            }
            startActivityForResult(intent, LAUNCH_MAP);
        };

        //Get items all items from server
        new Thread(() -> {

            try {
                list = Database.getAllItems();
                List<Item> newList = RemoteOperations.getAllItems();
                addItems(newList.stream().filter(e -> !list.contains(e)).collect(Collectors.toList()));
                updateItems(newList.stream().filter(e -> list.contains(e)).collect(Collectors.toList()));
                list = newList;
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            }

            runOnUiThread(() -> {
                CreateStoreItemAdapter = new CreateStoreItemAdapter(this, list);
                listView.setAdapter(CreateStoreItemAdapter);
            });
        }).start();
    }

    public void selectLocation(View view) {
        locationNotSelected.setVisibility(View.INVISIBLE);
        lockUI();
        locateUser();
    }

    public void lockUI() {
        locationButton.setVisibility(View.GONE);
        locationLoading.setVisibility(View.VISIBLE);
        createStore.setEnabled(false);

    }

    public void unlockUI() {
        locationButton.setVisibility(View.VISIBLE);
        locationLoading.setVisibility(View.INVISIBLE);
        createStore.setEnabled(true);
    }

    public void createStore(View view) {

        if (this.nameField.getText().toString().equals("")) {
            // Display error if no name provided
            error = true;
            nameNotSelected.setVisibility(View.VISIBLE);
            nameField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
        } else if (this.coords == null) {
            // Display error if location not selected
            locationNotSelected.setVisibility(View.VISIBLE);
        } else {

            HashMap<String,Double> checkItems = CreateStoreItemAdapter.getCheckedItems();
            new Thread(() -> {
                try {
                    RemoteOperations.createStore(this.nameField.getText().toString(),this.coords.getLatitude(), this.coords.getLongitude(), checkItems);
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                    );
                }

            }).start();

            Intent intent = new Intent();
            try {
                intent.putExtra(RESULT_STORE, new Store(this.nameField.getText().toString(), new Coordinates(this.coords.getLatitude(), this.coords.getLongitude())));
            } catch (InvalidCoordinatesException e) {
                // Ignore
            }

            intent.putExtra(RESULT_ITEMS, checkItems);

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @SuppressLint("MissingPermission")
    public void locateUser() {
        checkPermissions();
        locationManager.getCurrentLocation(LocationManager.GPS_PROVIDER,null, getMainExecutor(),consumer);
    }

    public void checkPermissions() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // If Location is disabled, prompt user to turn it ON
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionsIntent);
        }

        // If user hasn't granted Location Permissions, prompt for permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_MAP) {
            if(resultCode == Activity.RESULT_OK){
                coords = (Coordinates) data.getSerializableExtra("coords");
                String lat = Location.convert(coords.getLatitude(), FORMAT_DEGREES);
                String lon = Location.convert(coords.getLongitude(), FORMAT_DEGREES);
                String loc = lat + ", " + lon;
                coordsText.setText(loc);
                locationNotSelected.setVisibility(View.INVISIBLE);

            }
            unlockUI();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (error) {
            error = false;
            nameNotSelected.setVisibility(View.INVISIBLE);
            nameField.getBackground().mutate().setColorFilter(null);
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    private void addItems(List<Item> list) {
        Database.addItems(list);
    }

    private void updateItems(List<Item> list) {
        Database.updateItems(list);
    }

}