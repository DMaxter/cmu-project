package pt.ulisboa.tecnico.cmu.shopist.serializer;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import pt.ulisboa.tecnico.cmu.shopist.dto.ListSharedDto;

public class ListSharedDtoJsonSerializer implements JsonSerializer<ListSharedDto> {
    @Override
    public JsonElement serialize(ListSharedDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("users", context.serialize(dto.getUsers()));

        return obj;
    }

    public static String serialize(ListSharedDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(ListSharedDto.class, new ListSharedDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}