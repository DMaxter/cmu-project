package pt.ulisboa.tecnico.cmu.shopist.store;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Cart;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

public class StoreItemAdapter extends ArrayAdapter<ItemToBuy> {
    private final Context context;
    private final List<ItemToBuy> values;
    private final Cart cart;

    public StoreItemAdapter(Context context, List<ItemToBuy> values, Cart cart) {
        super(context, R.layout.adapter_store_items, values);
        this.context = context;
        this.values = values;
        this.cart = cart;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_store_items, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView priceView = rowView.findViewById(R.id.secondLine);
        ImageView imageView = rowView.findViewById(R.id.icon);
        TextView itemAmount = rowView.findViewById(R.id.item_ammount);

        Button addOneBtn = rowView.findViewById(R.id.add_one);
        Button addAllBtn = rowView.findViewById(R.id.add_all);

        // Add one button view and onClickListener
        if (values.get(position).getAmount() == 0) {
            addOneBtn.setEnabled(false);
        }

        addOneBtn.setTag(values.get(position));
        addOneBtn.setOnClickListener(v -> {
            ItemToBuy item = (ItemToBuy) v.getTag();
            cart.updateItemAmount(item, 1);
            int amount = item.getAmount();

            if (item.getPrice() == 0) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.price_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                final EditText userInput = (EditText) promptsView.findViewById(R.id.input_price_field);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                (dialog, id) -> {
                                    // get user input and set it to result
                                    // edit text
                                    Double price;
                                    if (!userInput.getText().toString().equals("")) {
                                        price = Double.parseDouble(userInput.getText().toString());
                                        priceView.setText(userInput.getText() + " €");
                                        item.setPrice(price);
                                        try {
                                            RemoteOperations.updateItemPrice(item.getStoreItem().getStore().getName(),item.getName(),item.getPrice());
                                            cart.setTotalCost(cart.getItemsInCart().stream().mapToDouble(e -> {
                                                if (e.getName().equals(item.getName())) {
                                                    e.getItemToBuy().setPrice(item.getPrice());
                                                    return e.getAmount() * item.getPrice();
                                                } else {
                                                    return e.getAmount() * e.getPrice();
                                                }
                                            }).sum());
                                            Intent intent = new Intent("updateCartTotal");
                                            intent.putExtra("newTotal", cart.getTotalCost());
                                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                        } catch (ServerUnreachableException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                        .setNegativeButton(R.string.cancel,
                                (dialog, id) -> dialog.cancel());

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            // Disable
            if (amount == 0) {
                addAllBtn.setEnabled(false);
                addOneBtn.setEnabled(false);
            }

            itemAmount.setText(String.valueOf(amount));
            Intent intent = new Intent("updateCartTotal");
            intent.putExtra("newTotal", cart.getTotalCost());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        });

        // Add one button view and onClickListener
        if (values.get(position).getAmount() == 0) {
            addAllBtn.setEnabled(false);
        }
        addAllBtn.setTag(values.get(position));
        addAllBtn.setOnClickListener(v -> {
            ItemToBuy item = (ItemToBuy) v.getTag();
            cart.updateItemAmount(item, item.getAmount());
            int amount = item.getAmount();

            if (item.getPrice() == 0) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.price_prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);
                final EditText userInput = (EditText) promptsView.findViewById(R.id.input_price_field);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                (dialog, id) -> {
                                    // get user input and set it to result
                                    // edit text
                                    Double price;
                                    if (!userInput.getText().toString().equals("")) {
                                        price = Double.parseDouble(userInput.getText().toString());
                                        priceView.setText(userInput.getText() + " €");
                                        item.setPrice(price);

                                        try {
                                            RemoteOperations.updateItemPrice(item.getStoreItem().getStore().getName(),item.getName(),item.getPrice());
                                            cart.setTotalCost(cart.getItemsInCart().stream().mapToDouble(e -> {
                                                if (e.getName().equals(item.getName())) {
                                                    e.getItemToBuy().setPrice(item.getPrice());
                                                    return e.getAmount() * item.getPrice();
                                                } else {
                                                    return e.getAmount() * e.getPrice();
                                                }
                                            }).sum());

                                            Intent intent = new Intent("updateCartTotal");
                                            intent.putExtra("newTotal", cart.getTotalCost());
                                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                        } catch (ServerUnreachableException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                        .setNegativeButton(R.string.cancel,
                                (dialog, id) -> dialog.cancel());

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            // Disable
            if (amount == 0) {
                addAllBtn.setEnabled(false);
                addOneBtn.setEnabled(false);
            }

            itemAmount.setText(String.valueOf(amount));
            Intent intent = new Intent("updateCartTotal");
            intent.putExtra("newTotal", cart.getTotalCost());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        });

        nameView.setText(values.get(position).getItem().getName());
        String price = values.get(position).getPrice() + " €";
        priceView.setText(price);
        itemAmount.setText(String.valueOf(values.get(position).getAmount()));

        imageView.setImageResource(R.mipmap.ic_launcher_foreground);

        return rowView;
    }

    public ItemToBuy getItem(int position) {
        return values.get(position);
    }
}
