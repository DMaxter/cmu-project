package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import pt.ulisboa.tecnico.cmu.shopist.relations.PantryItemCrossRef;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;

public class Converters {
    @TypeConverter
    public static List<String> restoreList(String list) {
        return new Gson().fromJson(list, new TypeToken<List<String>>() {}.getType());
    }

    @TypeConverter
    public static String saveList(List<String> list) {
        return new Gson().toJson(list);
    }

    @TypeConverter
    public static UUID restoreUUID(String id) {
        return UUID.fromString(id);
    }

    @TypeConverter
    public static String saveUUID(UUID id) {
        return id.toString();
    }

    @TypeConverter
    public static Coordinates restoreCoords(String coords) {
        return new Gson().fromJson(coords, new TypeToken<Coordinates>() {}.getType());
    }

    @TypeConverter
    public static String saveCoords(Coordinates coords) {
        return new Gson().toJson(coords);
    }

    @TypeConverter
    public static Map<String, PantryItemCrossRef> restoreItems(String items) {
        return new Gson().fromJson(items, new TypeToken<Map<String, PantryItemCrossRef>>() {}.getType());
    }

    @TypeConverter
    public static String saveItems(Map<String, PantryItemCrossRef> items) {
        return new Gson().toJson(items);
    }
}
