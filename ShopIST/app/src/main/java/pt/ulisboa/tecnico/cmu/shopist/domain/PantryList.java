package pt.ulisboa.tecnico.cmu.shopist.domain;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.UUID;

@Entity(tableName = "pantry_list")
public class PantryList implements Serializable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "pantry_id")
    private final UUID id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "owner")
    private final String owner;

    @NonNull
    @ColumnInfo(name = "coordinates")
    private Coordinates coords;

    public PantryList(@NonNull UUID id, @NonNull Coordinates coords, String name, String owner) {
        this.name = name;
        this.owner = owner;
        this.id = id;
        this.coords = coords;
    }

    public String getOwner() {
        return owner;
    }

    @NonNull
    public UUID getId() {
        return id;
    }

    public void setLocation(Coordinates coords) {
        this.coords = coords;
    }

    @NonNull
    public Coordinates getCoords() {
        return coords;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PantryList that = (PantryList) o;
        return id.equals(that.id);
    }
}
