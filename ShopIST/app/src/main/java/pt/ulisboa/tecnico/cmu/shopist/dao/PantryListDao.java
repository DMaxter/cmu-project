package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.UUID;

import pt.ulisboa.tecnico.cmu.shopist.domain.AmountInfo;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PantryListDao {
    @Insert
    void insert(PantryList list);

    @Insert
    void insertAll(List<PantryList> lists);

    @Delete
    void delete(PantryList list);

    @Delete
    void deleteAll(List<PantryList> lists);

    @Update
    void update(PantryList list);

    @Update
    void updateAll(List<PantryList> lists);

    @Query("SELECT * FROM pantry_list")
    List<PantryList> getAll();

    @Query("INSERT OR REPLACE INTO pantry_item (pantry_id, item_name, amount_shop, amount_pantry) VALUES (:listId, :itemName, :shop, :pantry)")
    void updateItemAmounts(UUID listId, String itemName, int shop, int pantry);

    @Query("DELETE FROM pantry_item WHERE pantry_id = :listId AND item_name = :itemName")
    void deleteItem(UUID listId, String itemName);

    @Query("SELECT * FROM pantry_list p INNER JOIN pantry_item JOIN item WHERE p.pantry_id = :listId")
    List<ItemInPantryList> getItems(UUID listId);

    @Query("UPDATE pantry_list SET owner = :username WHERE owner = :GUID")
    void registerOwner(String GUID, String username);
}
