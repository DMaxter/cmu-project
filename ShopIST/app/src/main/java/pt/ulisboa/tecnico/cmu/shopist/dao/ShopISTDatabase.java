package pt.ulisboa.tecnico.cmu.shopist.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.PantryItemCrossRef;
import pt.ulisboa.tecnico.cmu.shopist.relations.StoreItemCrossRef;

@Database(entities = {ItemInCart.class, Item.class, PantryItemCrossRef.class, PantryList.class, StoreItemCrossRef.class, Store.class},
        version = 1,
        exportSchema = false)
@TypeConverters(Converters.class)
public abstract class ShopISTDatabase extends RoomDatabase {
    public abstract ItemDao itemDao();

    public abstract PantryListDao pantryListDao();

    public abstract StoreDao storeDao();

    public abstract CartDao cartDao();
}
