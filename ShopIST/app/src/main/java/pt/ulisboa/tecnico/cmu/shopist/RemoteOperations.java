package pt.ulisboa.tecnico.cmu.shopist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import pt.ulisboa.tecnico.cmu.grpc.Shop;
import pt.ulisboa.tecnico.cmu.grpc.Shop.*;
import pt.ulisboa.tecnico.cmu.grpc.ShopServiceGrpc;
import pt.ulisboa.tecnico.cmu.grpc.ShopServiceGrpc.ShopServiceBlockingStub;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInCart;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.Store;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.*;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInStore;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemToBuy;

public class RemoteOperations {
    private static ShopServiceBlockingStub stub;
    private static final String host = "shopist.gq";
    private static final int    port =  1337;
    private static Logger logger;

    private RemoteOperations() {}

    /**
     * Initialize the communication channel to server
     */
    public static void init() {
        logger = Logger.getLogger(RemoteOperations.class.getSimpleName());
        stub = ShopServiceGrpc.newBlockingStub(ManagedChannelBuilder.forAddress(host, port)
                .build());
    }

    /**
     * Register a named user
     *
     * @param username - the username to associate to this account
     * @param guid - the GUID of the current phone
     * @param hash - the hash of the GUID
     * @throws UserAlreadyExistsException if a user already exists with the given username
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void register(String username, String guid, String hash) throws ServerUnreachableException, UserAlreadyExistsException {
        try {
            stub.register(RegisterRequest.newBuilder().setUsername(username).setHash(hash).setGuid(guid).build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't login to server");
                throw new UserAlreadyExistsException();
            } else {
                logger.info("Exception received at login: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }

    /**
     * Login as named user
     *
     * @param username - the username of the user to log in
     * @param hash - the hash of the user's password
     * @throws InvalidLoginException if user/pass doesn't match or username doesn't exist
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void login(String username, String hash) throws InvalidLoginException, ServerUnreachableException {
        try {
            stub.login(LoginRequest.newBuilder().setUsername(username).setHash(hash).build());
        } catch (StatusRuntimeException e) {
           if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
               logger.info("Couldn't login to server");
               throw new InvalidLoginException();
           } else {
               logger.info("Exception received at login: " + e.getStatus().toString());
               throw new ServerUnreachableException();
           }
        }
    }

    /**
     * Login as a guest user
     *
     * @param guid - the GUID of the current phone
     * @param hash - the hash of the GUID
     * @throws InvalidLoginException if GUID doesn't exist
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void guestLogin(String guid, String hash) throws InvalidLoginException, ServerUnreachableException {
        try {
            stub.guest(GuestRequest.newBuilder().setGuid(guid).setHash(hash).build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't login to server");
                throw new InvalidLoginException();
            } else {
                logger.info("Exception received at login: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }


    //
    // List-related operations
    //

    /**
     * Create new pantry list
     *
     * @param username - the username of current user
     * @param list_name - the name of the new pantry list
     * @param latitude - the latitude (coordinates of the list)
     * @param longitude - the longitude (coordinates of the list)
     * @throws InvalidListException if the coordinates are invalid or user is not registered
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void createPantryList(String username, String list_id, String list_name, double latitude, double longitude) throws InvalidListException, ServerUnreachableException {
        try {
            stub.createList(CreateListRequest.newBuilder()
                    .setId(Shop.UUID.newBuilder().setValue(list_id).build())
                    .setUsername(username)
                    .setListName(list_name)
                    .setLatitude(latitude).setLongitude(longitude)
                    .build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't create new pantry list");
                throw new InvalidListException();
            } else {
                logger.info("Exception received at create pantry list: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }

    /**
     * Edit pantry list
     *
     * @param username - the username of current user (must be owner)
     * @param list_id - id of the list to be edited
     * @param latitude - the latitude (coordinates of the list)
     * @param longitude - the longitude (coordinates of the list)
     * @throws InvalidListException if the coordinates are invalid or user not owner
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void editPantryList(String username, String list_id, String newName, double latitude, double longitude) throws InvalidListException, ServerUnreachableException {
        try {
            stub.editList(EditListRequest.newBuilder()
                    .setOwner(username)
                    .setListName(newName)
                    .setListId(Shop.UUID.newBuilder().setValue(list_id).build())
                    .setLatitude(latitude).setLongitude(longitude)
                    .build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't edit pantry list");
                throw new InvalidListException();
            } else {
                logger.info("Exception received in edit pantry list: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }

    /**
     * Delete a pantry list
     *
     * @param username - the username of current user
     * @param list - the pantry list to delete
     * @throws UserNotOwnerException if the coordinates are invalid or user is not registered
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void deletePantryList(String username, PantryList list) throws UserNotOwnerException, ServerUnreachableException {
        try {
            stub.deleteList(DeleteListRequest.newBuilder()
                    .setUsername(username)
                    .setListId(Shop.UUID.newBuilder().setValue(list.getId().toString()).build())
                    .build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't delete the pantry list");
                throw new UserNotOwnerException();
            } else {
                logger.info("Exception received at delete pantry list: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }

    /**
     * Get all lists from user
     *
     * @param username - the username of current user
     * @return the metadata of the lists belonging to the user
     * @throws InvalidListException if the coordinates are invalid or user is not registered
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static List<PantryList> getPantryLists(String username, String hash) throws AlreadyUpdatedException, InvalidListException, ServerUnreachableException {
        try {
            GetListsResponse response = stub.getLists(GetListsRequest.newBuilder().setUsername(username).setHash(hash).build());
            if (response.hasResult()) {
                List<PantryList> lists = new ArrayList<>();
                for (ItemList item_list : response.getResult().getListList()) {
                    UUID id = UUID.fromString(item_list.getId().getValue());
                    Coordinates coords = new Coordinates(item_list.getLatitude(), item_list.getLongitude());

                    lists.add(new PantryList(id, coords, item_list.getName(), item_list.getOwner()));
                }

                return lists;
            } else {
                throw new AlreadyUpdatedException();
            }
        } catch (StatusRuntimeException  e) {
            logger.info("Exception received at get pantry lists: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        } catch (InvalidCoordinatesException e) {
            logger.info("Exception received at get pantry lists: ");
            throw new InvalidListException();
        }
    }

    /**
     * Get the users with whom the given list is shared
     *
     * @param list_id - the id of the list
     * @param username - the username of the owner of the list
     * @param hash - the hash of the list of users
     * @return a list with the names of the users with whom the list is shared
     * @throws AlreadyUpdatedException if the information is already updated on the client side
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static List<String> getUsersFromPantry(String list_id, String username, String hash) throws AlreadyUpdatedException, ServerUnreachableException {
        try {
            GetSharedUsersResponse response = stub.getSharedUsers(GetSharedUsersRequest.newBuilder()
                    .setListId(Shop.UUID.newBuilder().setValue(list_id).build())
                    .setOwner(username)
                    .setHash(hash)
                    .build());

            if (response.hasResult()) {
                return response.getResult().getUsernameList().stream().collect(Collectors.toList());
            } else {
                throw new AlreadyUpdatedException();
            }
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at get users from pantry: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Get all items from specific list
     *
     * @param pantry - the pantry list to get the items from
     * @param hash - the hash of the pantry list
     * @return the metadata of the items in the list
     * @throws ServerUnreachableException if the server could not be reached
     * @throws AlreadyUpdatedException if the information is already updated on the client side
     */
    public static List<ItemInPantryList> getListItems(PantryList pantry, String hash) throws AlreadyUpdatedException, ServerUnreachableException {
        try {
            GetListResponse response = stub.getList(GetListRequest.newBuilder()
                    .setListId(Shop.UUID.newBuilder().setValue(pantry.getId().toString()).build())
                    .setHash(hash)
                    .build());

            if (response.hasResult()) {
                List<ItemInPantryList> list = new ArrayList<>();
                for (Shop.Item i : response.getResult().getItemList()) {
                    list.add(new ItemInPantryList(pantry, new Item(i.getName(), i.getBarcode()), i.getPantryAmount(), i.getShopAmount()));
                }

                return list;
            } else {
                throw new AlreadyUpdatedException();
            }
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at get pantry lists: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Share list with given user
     *
     * @param username_sharing - the user that is sharing the list
     * @param username_to_share - the user that is going to be added to the share
     * @param list_id - the id of the list to be shared
     * @throws UserNotFoundException
     * @throws ServerUnreachableException
     */
    public static void shareList(String username_sharing, String username_to_share, String list_id) throws UserNotFoundException, ServerUnreachableException {
        try {
            stub.shareList(ShareListRequest.newBuilder()
                    .setUsernameSharing(username_sharing)
                    .setUsernameToShare(username_to_share)
                    .setListId(Shop.UUID.newBuilder()
                            .setValue(list_id))
                    .build());
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.INVALID_ARGUMENT.getCode()) {
                logger.info("Couldn't share the pantry list");
                throw new UserNotFoundException();
            } else {
                logger.info("Exception received at share pantry list: " + e.getStatus().toString());
                throw new ServerUnreachableException();
            }
        }
    }

    /**
     * Unshare list with given user
     *
     * @param username_unsharing - the user that is unsharing the list
     * @param username_to_unshare - the user that is going to be removed from the share
     * @param list_id - the id of the list that is going to be unshared
     */
    public static void unshareList(String username_unsharing, String username_to_unshare, String list_id) throws ServerUnreachableException {
        try {
            stub.unshareList(UnshareListRequest.newBuilder()
                    .setUsernameUnsharing(username_unsharing)
                    .setUsernameToUnshare(username_to_unshare)
                    .setListId(Shop.UUID.newBuilder()
                            .setValue(list_id)
                            .build())
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at get unshare list: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Remove an item from a pantry list
     *
     * @param username - the user trying to delete the item
     * @param list_id - the list to delete the item from
     * @param item - the item to be removed
     * @throws ServerUnreachableException if the server could not be reached
     * @throws UserNotFoundException if the user doesn't exist
     * @throws UserNotOwnerException if the user trying to delete the item is not the owner of the list
     */
    public static void removeItemFromList(String username, String list_id, String item) throws ServerUnreachableException, UserNotFoundException, UserNotOwnerException {
        try {
            stub.removeItemFromList(RemoveItemRequest.newBuilder().setItemName(item).setListId(Shop.UUID.newBuilder().setValue(list_id)).setUsername(username).build());
        } catch (StatusRuntimeException e) {
            Status status = e.getStatus();
            if (status.getCode() == Status.Code.INVALID_ARGUMENT) {
                throw new UserNotFoundException();
            } else if(status.getCode() == Status.Code.FAILED_PRECONDITION) {
                throw new UserNotOwnerException();
            } else {
                throw new ServerUnreachableException();
            }
        }
    }

    //
    // Item-related operations
    //

    /**
     * Create a new item
     *
     * @param item - the item to create
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void createItem(Item item) throws ServerUnreachableException {
        try {
            stub.createItem(CreateItemRequest.newBuilder()
                    .setName(item.getName())
                    //.setPicture(item.getPictures().get(0))
                    .setBarcode(item.getBarcode())
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Update an item amount in a specific list
     *
     * @param list - the pantry list id to update
     * @param old_item_name - the old name of item to update
     * @param new_item_name - the new name of item to update
     * @param shop - the amount to buy
     * @param pantry - the amount in pantry
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void updateItemDetails(String list, String old_item_name, String new_item_name, String barcode, int shop, int pantry) throws ServerUnreachableException {
        try {
            stub.updateItemDetails(UpdateItemDetailsRequest.newBuilder()
                    .setListId(Shop.UUID.newBuilder().setValue(list).build())
                    .setNewItemName(new_item_name)
                    .setOldItemName(old_item_name)
                    .setBarcode(barcode)
                    .setPantryAmount(pantry)
                    .setShopAmount(shop)
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Update an item amount in a specific list
     *
     * @param list - the pantry list id to update
     * @param name - the name of item to update
     * @param shop - the amount to buy
     * @param pantry - the amount in pantry
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void updateItemAmount(String list, String name, int shop, int pantry) throws ServerUnreachableException {
        try {
            stub.updateItemAmount(UpdateItemAmountRequest.newBuilder()
                    .setListId(Shop.UUID.newBuilder().setValue(list).build())
                    .setPantryAmount(pantry)
                    .setShopAmount(shop)
                    .setItemName(name)
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Rates an item
     *
     * @param username - name of the user rating the item
     * @param item_name - the name of the item to be rated
     * @param rating - the rating given by the user
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void rateItem(String username, String item_name, int rating) throws ServerUnreachableException {
        try {
            stub.rateItem(RateItemRequest.newBuilder()
                    .setUsername(username)
                    .setItem(item_name)
                    .setRating(rating)
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Gets all the ratings of a specific item
     *
     * @param item_name
     * @return
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static List<Integer> getItemRatings(String item_name) throws ServerUnreachableException {
        try {
            GetItemRatingResponse response = stub.getItemRating(GetItemRatingRequest.newBuilder()
                    .setItem(item_name)
                    .build());

            return response.getRatingsList();
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Checks if user can rate an item
     *
     * @param username - name of the user
     * @param item_name - name of the item
     * @return - true if the user can rate the item, false if the user has already done it
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static Boolean canRateItem(String username, String item_name) throws ServerUnreachableException {
        try {
            CanRateItemResponse response = stub.canRateItem(CanRateItemRequest.newBuilder()
                    .setUsername(username)
                    .setItem(item_name)
                    .build());

            return response.getCanRate();
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at update item quantity: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Add a picture of an item
     *
     * @param item - the item to update
     * @param picture - the picture to add
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void addItemPicture(Item item, String picture) throws ServerUnreachableException {
        try {
            stub.addItemPicture(AddItemPictureRequest.newBuilder()
                    .setName(item.getName())
                    .setPicture(picture)
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at add item picture: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Add a list of items to a store
     *
     * @param store - the new store
     * @param items - the list of items
     * @throws ServerUnreachableException if the server could not be reached
     */
    public static void addItemsToStore(String store, HashMap<String, Double> items) throws ServerUnreachableException {
        try {
            AddItemsToStoreRequest.Builder request = AddItemsToStoreRequest.newBuilder()
                    .setStore(store);

            for (Map.Entry<String, Double> entry: items.entrySet()) {
                request.addItems(Shop.Item.newBuilder().setName(entry.getKey()).setPrice(entry.getValue()));
            }

            stub.addItemsToStore(request.build());

        } catch (StatusRuntimeException e) {
            logger.info("Exception received at add item picture: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Creates a store including possible initially available items
     *
     * @param store_name - name of the store
     * @param latitude - latitude of the store
     * @param longitude - longitude of the store
     * @param items - list of initially available items (and possibly their price)
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void createStore(String store_name, double latitude, double longitude, HashMap<String, Double> items) throws ServerUnreachableException {
        try {
            CreateStoreRequest.Builder request = CreateStoreRequest.newBuilder()
                    .setName(store_name)
                    .setLatitude(latitude)
                    .setLongitude(longitude);

            for (Map.Entry<String, Double> entry: items.entrySet()) {
                request.addItem(Shop.Item.newBuilder().setName(entry.getKey()).setPrice(entry.getValue()));
            }
            stub.createStore(request.build());
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at create store: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Gets the metadata of all the stores currently stored in the server
     *
     * @param username - name of the user
     * @return - a list of stores (only their metadata)
     * @throws InvalidCoordinatesException if the coordinates are invalid
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static List<Store> getStores(String username) throws InvalidCoordinatesException, ServerUnreachableException {
        try {
            GetStoresResponse response = stub.getStores(GetStoresRequest.newBuilder().setUsername(username).build());

            ArrayList<Store> stores = new ArrayList<>();

            for (Shop.Store store : response.getStoresList()) {
                stores.add(new Store(store.getName(), new Coordinates(store.getLatitude(), store.getLongitude()), store.getWaitTime()));
            }
            return stores;
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at get stores: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Gets a filtered view based on the items the user needs and the ones available at a specific store
     *
     * @param username - name of the user
     * @param store - name of the store
     * @return - list of items that the user can buy at that store
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static List<ItemToBuy> getStoreView(String username, Store store) throws ServerUnreachableException {
        try {
            GetStoreViewResponse response = stub.getStoreView(GetStoreViewRequest.newBuilder()
                    .setUsername(username)
                    .setStoreName(store.getName())
                    .build());

            List<ItemToBuy> items = new ArrayList<>();
            for (Shop.Item item: response.getItemsList()) {
                items.add(new ItemToBuy(new ItemInStore(store, new Item(item.getName(), item.getBarcode()), item.getPrice()), item.getShopAmount()));
            }
            return items;
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at get store view: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Gets all items currently stored in the server
     *
     * @return - the list of all the items in the server
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static List<Item> getAllItems() throws ServerUnreachableException {
        try {
            GetAllItemsResponse response = stub.getAllItems(GetAllItemsRequest.newBuilder().build());

            ArrayList<Item> items = new ArrayList<>();
            for (Shop.Item item: response.getItemsList()) {
                items.add(new Item(item.getName(), item.getBarcode()));
            }
            return items;
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at get all items: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Buys all the items present in the cart of the user (fills pantry lists)
     *
     * @param store_name - name of the store
     * @param username - name of the user
     * @param items - list of items that the user has bought in that store
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void checkoutStore(String store_name, String username, List<ItemInCart> items) throws ServerUnreachableException {
        try {
            CheckoutStoreRequest.Builder request = CheckoutStoreRequest.newBuilder()
                    .setStore(store_name)
                    .setUsername(username);

            for (ItemInCart item: items) {
                request.addItems(Shop.Item.newBuilder().setName(item.getName()).setShopAmount(-item.getAmount()));
            }
            stub.checkoutStore(request.build());
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at checkout store: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Gets all the items that are not yet in the store
     *
     * @param store_name - name of the store
     * @return - the list of the items not present in the store
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static List<Item> getItemsNotInStore(String store_name) throws ServerUnreachableException {
        try {
            GetItemsNotInStoreResponse response = stub.getItemsNotInStore(GetItemsNotInStoreRequest.newBuilder().setStore(store_name).build());

            ArrayList<Item> items = new ArrayList<>();
            for (String item_name: response.getItemNameList()) {
                items.add(new Item(item_name));
            }
            return items;
        }
        catch (StatusRuntimeException e) {
            logger.info("Exception received at get items not in store: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Updates the price of an item in a specific store
     *
     * @param store - name of the store
     * @param item - name of the item
     * @param price - new price
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void updateItemPrice(String store, String item, double price) throws ServerUnreachableException {
        try {
            stub.updateItemPrice(UpdateItemPriceRequest.newBuilder()
                    .setStore(store)
                    .setItem(item)
                    .setPrice(price)
                    .build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at add item picture: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Informs the server that the user has entered the queue
     *
     * @param store_name - name of the store
     * @param username - name of the user
     * @param items - list of items the user has to buy
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void enterQueue(String store_name, String username, int items) throws ServerUnreachableException {
        try {
            stub.enterQueue(EnterQueueRequest.newBuilder().setStore(store_name).setUsername(username).setNumItems(items).build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at enter queue: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }

    /**
     * Informs the server that the user has left the queue
     *
     * @param store_name - the name of the store
     * @param username - the name of the user
     * @throws ServerUnreachableException if the server could not be reached or there was some error in the server
     */
    public static void leaveQueue(String store_name, String username) throws ServerUnreachableException {
        try {
            stub.leaveQueue(LeaveQueueRequest.newBuilder().setStore(store_name).setUsername(username).build());
        } catch (StatusRuntimeException e) {
            logger.info("Exception received at leave queue: " + e.getStatus().toString());
            throw new ServerUnreachableException();
        }
    }
}