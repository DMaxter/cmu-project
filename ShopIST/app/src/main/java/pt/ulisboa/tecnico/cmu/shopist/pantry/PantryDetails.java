package pt.ulisboa.tecnico.cmu.shopist.pantry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.Hasher;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.Coordinates;
import pt.ulisboa.tecnico.cmu.shopist.domain.Item;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.dto.PantryDto;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.AlreadyUpdatedException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserNotFoundException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserNotOwnerException;
import pt.ulisboa.tecnico.cmu.shopist.item.CreateItem;
import pt.ulisboa.tecnico.cmu.shopist.item.EditItem;
import pt.ulisboa.tecnico.cmu.shopist.item.ItemListAdapter;
import pt.ulisboa.tecnico.cmu.shopist.item.ItemRatings;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;
import pt.ulisboa.tecnico.cmu.shopist.serializer.PantryDtoJsonSerializer;

import static android.location.Location.FORMAT_DEGREES;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_GUID;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_LOGGED;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_NAME;
import static pt.ulisboa.tecnico.cmu.shopist.MainActivity.PREF_USER;

public class PantryDetails extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    private TextView pantryNameView;
    private TextView pantryCoordsView;
    private PantryList pantryList;
    private ListView listView;
    private ImageButton menuButton;
    private String user;
    private boolean isOwner = true;
    private ActionMode.Callback actionModeCallback;
    private ItemInPantryList selected;
    private int selectedIndex = -1;
    private List<ItemInPantryList> items;
    private ImageView mapImage;
    private SwipeRefreshLayout swipe;

    private static final String INTENT_ITEM_POS = "ItemPos";

    private static final int ITEM_RATINGS = 4;
    private static final int EDIT_PANTRY = 3;
    private static final int CREATE_ITEM = 2;
    private static final int EDIT_ITEM = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantry_details);
        pantryList = (PantryList) getIntent().getSerializableExtra("pantryList");
        listView = findViewById(R.id.list_view);
        mapImage = findViewById(R.id.pantryImage);
        swipe = findViewById(R.id.swiperefresh);

        pantryNameView = findViewById(R.id.pantry_list_name);
        pantryCoordsView = findViewById(R.id.pantry_list_coordinates);
        menuButton = findViewById(R.id.menu_btn);
        user = loadUser();
        isOwner = checkOwnership();

        swipe.setOnRefreshListener(this::showPantryDetails);

        actionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.edit_delete_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @SuppressLint("NonConstantResourceId")
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                String name = selected.getItem().getName();
                switch (item.getItemId()) {
                    case R.id.delete_icon:
                        new AlertDialog.Builder(PantryDetails.this)
                                .setTitle("Delete " + selected.getItem().getName())
                                .setMessage("Do you really want to delete " + name + " ?")
                                .setIcon(R.drawable.ic_warning_black_24dp)
                                .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> new Thread(() -> {
                                    SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                                    boolean logged = settings.getBoolean(PREF_LOGGED, false);
                                    String user;
                                    if (logged) {
                                        user = settings.getString(PREF_USER, "");
                                    } else {
                                        user = settings.getString(PREF_GUID, "");
                                    }

                                    deleteItem(selected);

                                    try {
                                        RemoteOperations.removeItemFromList(user, pantryList.getId().toString(), name);
                                        items.remove(selectedIndex);
                                        runOnUiThread(() -> showPantryDetails());
                                    } catch (ServerUnreachableException | UserNotOwnerException | UserNotFoundException e) {
                                        runOnUiThread(() -> Toast.makeText(getApplicationContext(), "An error occurred. Try again later.", Toast.LENGTH_SHORT).show());
                                    }
                                }).start())
                                .setNegativeButton(android.R.string.cancel, null).show();
                        mode.finish();
                        return true;
                    case R.id.edit_icon:
                        Intent intent = new Intent(PantryDetails.this, EditItem.class);
                        intent.putExtra(INTENT_ITEM_POS, selectedIndex);
                        intent.putExtra(EditItem.INTENT_ITEM_NAME, name);
                        intent.putExtra(EditItem.INTENT_ITEM_AMOUNT, selected);
                        startActivityForResult(intent, EDIT_ITEM);
                        mode.finish();
                        return true;
                    case R.id.ratings_icon:
                        Intent intent1 = new Intent(PantryDetails.this, ItemRatings.class);
                        intent1.putExtra(INTENT_ITEM_POS, selectedIndex);
                        intent1.putExtra(EditItem.INTENT_ITEM_NAME, name);
                        intent1.putExtra(EditItem.INTENT_ITEM_AMOUNT, selected);
                        startActivityForResult(intent1, ITEM_RATINGS);
                        mode.finish();
                        return true;
                    case R.id.share_icon:
                        String barcode = selected.getItem().getBarcode();
                        Log.i("BARCODE", "Barcode is" + barcode);
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        String text = "This item is amazing! Check it out: " + name;
                        if (!barcode.equals("")) {
                            text += " (" + barcode + ") ";
                        }
                        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                        sendIntent.setType("text/plain");

                        Intent shareIntent = Intent.createChooser(sendIntent, null);
                        startActivity(shareIntent);
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        };
        loadFromDB();
    }

    public void showPantryDetails() {
        if (isOwner) {
            pantryNameView.setText(pantryList.getName());
        } else {
            menuButton.setVisibility(View.INVISIBLE);
            menuButton.setEnabled(false);
            String name = pantryList.getName() + " (Shared by " + pantryList.getOwner() + ")";
            pantryNameView.setText(name);
        }

        String lat = Location.convert(pantryList.getCoords().getLatitude(), FORMAT_DEGREES);
        String lon = Location.convert(pantryList.getCoords().getLongitude(), FORMAT_DEGREES);
        String loc = lat + ", " + lon;
        pantryCoordsView.setText(loc);

        getGoogleMapThumbnail(pantryList.getCoords());

        Thread t = new Thread(() -> {
            try {
                List<ItemInPantryList> newList = RemoteOperations.getListItems(pantryList, Hasher.hash(PantryDtoJsonSerializer.serialize(new PantryDto(pantryList, items))));
                deleteItems(items.stream().filter(e -> !newList.contains(e)).collect(Collectors.toList()));
                addItems(newList.stream().filter(e -> !items.contains(e)).collect(Collectors.toList()));
                updateItemsAmount(newList.stream().filter(e -> items.contains(e)).collect(Collectors.toList()));
                items = newList;
            } catch (AlreadyUpdatedException ignored) {
            } catch (ServerUnreachableException e) {
                runOnUiThread(() ->
                        Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                );
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayAdapter<ItemInPantryList> itemListAdapter = new ItemListAdapter(this, this.items);
        listView.setAdapter(itemListAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            selected = itemListAdapter.getItem(position);
            selectedIndex = position;
            view.setSelected(true);
            startActionMode(actionModeCallback);
        });
        swipe.setRefreshing(false);
    }

    public void getGoogleMapThumbnail(Coordinates location) {
        new Thread(() -> {
            try {
                URL url = new URL("https://maps.googleapis.com/maps/api/staticmap?center=" + location.getLatitude() + "," + location.getLongitude() + "&markers=color:red%7C" + location.getLatitude() + "," + location.getLongitude() + "&zoom=15&size=250x250&sensor=false&key=AIzaSyAy6vSql-qRt9yvqBzejSWMl5hMfR5pxdk");
                HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                final Bitmap bmp = BitmapFactory.decodeStream(in);
                runOnUiThread(() -> mapImage.setImageBitmap(bmp));
                in.close();
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, CreateItem.class);
        startActivityForResult(intent, CREATE_ITEM);
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.pantry_popup_menu);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_pantry_op:
                Intent intentEdit = new Intent(this, EditPantry.class);
                intentEdit.putExtra("pantryToEdit", pantryList);
                startActivityForResult(intentEdit, EDIT_PANTRY);
                return true;
            case R.id.share_pantry_op:
                Intent intentUnshare = new Intent(this, UnsharePantry.class);
                intentUnshare.putExtra("pantryToUnshare", pantryList);
                startActivity(intentUnshare);
                return true;
            case R.id.delete_pantry_op:
                deletePantry();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CREATE_ITEM) {
                Item item = (Item) data.getSerializableExtra(CreateItem.RESULT_ITEM);
                int amountShop = data.getIntExtra(CreateItem.RESULT_ITEM_AMOUNT_SHOP, 0);
                int amountPantry = data.getIntExtra(CreateItem.RESULT_ITEM_AMOUNT_PANTRY, 0);

                ItemInPantryList pantryItem = new ItemInPantryList(pantryList, item, amountPantry, amountShop);

                new Thread(() -> {
                    items.add(pantryItem);
                    addItem(pantryItem);


                    try {
                        RemoteOperations.createItem(item);
                        RemoteOperations.updateItemAmount(pantryList.getId().toString(), item.getName(), amountShop, amountPantry);
                    } catch (ServerUnreachableException e) {
                        runOnUiThread(() ->
                                Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                        );
                    }
                    runOnUiThread(this::showPantryDetails);
                }).start();
            } else if (requestCode == EDIT_ITEM) {
                ItemInPantryList item = (ItemInPantryList) data.getSerializableExtra(EditItem.RESULT_ITEM_AMOUNT);

                items.set(data.getIntExtra(INTENT_ITEM_POS, 0), item);

                new Thread(() -> updateItemAmount(item)).start();

                try {
                    RemoteOperations.updateItemAmount(pantryList.getId().toString(), item.getItem().getName(), item.getAmountShop(), item.getAmountPantry());
                } catch (ServerUnreachableException e) {
                    runOnUiThread(() ->
                            Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_LONG).show()
                    );
                }
                runOnUiThread(this::showPantryDetails);

            } else if (requestCode == EDIT_PANTRY) {
                pantryList = (PantryList) data.getSerializableExtra(EditPantry.NEW_PANTRY);

                new Thread(() -> {
                    updatePantry(pantryList);
                    runOnUiThread(this::showPantryDetails);
                }).start();
            }
        }
    }

    public void deletePantry() {
        new AlertDialog.Builder(this)
                .setTitle("Delete " + pantryList.getName())
                .setMessage("Do you really want to delete " + pantryList.getName() + " ?")
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> new Thread(() -> {
                    SharedPreferences settings = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                    boolean logged = settings.getBoolean(PREF_LOGGED, false);
                    String user;
                    if (logged) {
                        user = settings.getString(PREF_USER, "");
                    } else {
                        user = settings.getString(PREF_GUID, "");
                    }

                    deletePantry(pantryList);

                    try {
                        RemoteOperations.deletePantryList(user, pantryList);
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();

                    } catch (ServerUnreachableException | UserNotOwnerException e) {
                        runOnUiThread(() -> Toast.makeText(getApplicationContext(), "An error occurred. Try again later.", Toast.LENGTH_SHORT).show());
                    }
                }).start())
                .setNegativeButton(android.R.string.cancel, null).show();
    }

    public void showLocation(View view) {
        Intent intent = new Intent(this, PantryMapView.class);
        intent.putExtra("pantryCoords", pantryList.getCoords());
        startActivity(intent);
    }

    public boolean checkOwnership() {
        return user.equals(pantryList.getOwner());
    }

    public String loadUser() {
        SharedPreferences settings = getSharedPreferences("Login", Context.MODE_PRIVATE);
        boolean logged = settings.getBoolean("Logged", false);
        String user;
        if (logged) {
            user = settings.getString("Username", "");
        } else {
            user = settings.getString("GUID", "");
        }
        return user;
    }


    private void updatePantry(PantryList pantry) {
        Database.updatePantryList(pantry);
    }

    private void deletePantry(PantryList pantry) {
        Database.deletePantryList(pantry);
    }

    private void addItem(ItemInPantryList item) {
        if (Database.hasItem(item.getItem().getName())) {
            Item dbItem = Database.getItem(item.getItem().getName());
            item.setItem(dbItem);
        } else {
            Database.addItem(item.getItem());
        }
        addItemAmount(item);
    }

    private void addItems(List<ItemInPantryList> list) {
        list.forEach(e -> {
            if (Database.hasItem(e.getItem().getName())) {
                Database.updateItem(e.getItem());
                updateItemAmount(e);
            } else {
                addItem(e);
            }
        });
    }

    private void addItemAmount(ItemInPantryList item) {
        Database.updateItemAmount(pantryList.getId(), item.getItem().getName(), item.getAmountShop(), item.getAmountPantry());
    }

    private void updateItemAmount(ItemInPantryList item) {
        addItemAmount(item);
    }

    private void updateItemsAmount(List<ItemInPantryList> list) {
        list.forEach(this::addItemAmount);
    }

    private void deleteItem(ItemInPantryList item) {
        Database.deleteItemFromPantry(pantryList.getId(), item.getItem().getName());
    }

    private void deleteItems(List<ItemInPantryList> list) {
        list.forEach(this::deleteItem);
    }

    private void loadFromDB() {
        new Thread(() -> {
            items = Database.getItemsInPantry(pantryList.getId());
            runOnUiThread(this::showPantryDetails);
        }).start();
    }
}