package pt.ulisboa.tecnico.cmu.shopist.domain;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "store")
public class Store implements Serializable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "store_name")
    private final String name;

    @NonNull
    @ColumnInfo(name = "coordinates")
    private final Coordinates coords;

    @Ignore
    private long time = -1;

    public Store(@NonNull String name, @NonNull Coordinates coords, long time) {
        this.name = name;
        this.coords = coords;
        this.time = time;
    }

    public Store(@NonNull String name, @NonNull Coordinates coords) {
        this.name = name;
        this.coords = coords;
    }

    public Coordinates getCoords() {
        return this.coords;
    }

    @NonNull
    public String getName() { return this.name; }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Store that = (Store) o;
        return name.equals(that.name);
    }
}
