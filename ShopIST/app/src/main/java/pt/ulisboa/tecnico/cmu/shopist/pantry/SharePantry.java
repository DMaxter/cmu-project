package pt.ulisboa.tecnico.cmu.shopist.pantry;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.domain.PantryList;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserNotFoundException;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.UserNotOwnerException;

import static com.google.common.collect.ComparisonChain.start;

public class SharePantry extends AppCompatActivity implements TextWatcher {
    private TextView share_txt;
    private PantryList pantryList;
    private EditText codeField;
    private TextView errorText;
    private Button shareButton;
    private boolean error = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_pantry);

        shareButton = findViewById(R.id.confirm_share_btn);
        errorText = findViewById(R.id.code_error_txt);
        codeField = findViewById(R.id.input_code_field);
        codeField.addTextChangedListener(this);
        share_txt = findViewById(R.id.share_pantry_txt);
        pantryList = (PantryList) getIntent().getSerializableExtra("pantryToShare");
        String text = "Share \"" + pantryList.getName() + "\"";
        share_txt.setText(text);
    }

    public void readQR(View view) {
        IntentIntegrator qrScan = new IntentIntegrator(this);
        qrScan.setOrientationLocked(false);
        qrScan.addExtra("SCAN_FORMATS","QR_CODE");
        qrScan.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,
                resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Context context = getApplicationContext();
                CharSequence text = "Error reading QR Code...";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            } else {
                String username = result.getContents();
                codeField.setText(username);

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void sharePantry(View view) {
        new Thread(() -> {
            SharedPreferences settings = getSharedPreferences("Login", Context.MODE_PRIVATE);
            boolean logged = settings.getBoolean("Logged", false);
            String owner;
            if (logged) {
                owner = settings.getString("Username", "");
            }
            else {
                owner = settings.getString("GUID", "");
            }

            String user = codeField.getText().toString();

            try {
                RemoteOperations.shareList(owner, user, pantryList.getId().toString());
                runOnUiThread(() -> {
                    setResult(Activity.RESULT_OK, new Intent());
                    finish();
                });
            } catch (UserNotFoundException e) {
                runOnUiThread(() -> {
                    error = true;
                    errorText.setVisibility(View.VISIBLE);
                    codeField.getBackground().mutate().setColorFilter(new BlendModeColorFilter(getColor(android.R.color.holo_red_dark), BlendMode.SRC_ATOP));
                });
            } catch (ServerUnreachableException e) {
                runOnUiThread(() -> {
                    Toast.makeText(getApplicationContext(), "Couldn't connect to server", Toast.LENGTH_SHORT).show();
                });

            }
        }).start();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (error) {
            codeField.getBackground().mutate().setColorFilter(null);
            errorText.setVisibility(View.INVISIBLE);
        } else if (codeField.getText().toString().equals("")){
            shareButton.setEnabled(false);
        } else {
            shareButton.setEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_OK);
        finish();
    }
}