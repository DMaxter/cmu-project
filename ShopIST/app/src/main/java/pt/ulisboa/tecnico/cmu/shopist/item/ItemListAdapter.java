package pt.ulisboa.tecnico.cmu.shopist.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pt.ulisboa.tecnico.cmu.shopist.Database;
import pt.ulisboa.tecnico.cmu.shopist.R;
import pt.ulisboa.tecnico.cmu.shopist.RemoteOperations;
import pt.ulisboa.tecnico.cmu.shopist.exceptions.ServerUnreachableException;
import pt.ulisboa.tecnico.cmu.shopist.relations.ItemInPantryList;

public class ItemListAdapter extends ArrayAdapter<ItemInPantryList> {
    private final Context context;
    private final List<ItemInPantryList> values;

    public ItemListAdapter(Context context, List<ItemInPantryList> items) {
        super(context, R.layout.adapter_item_list, items);
        this.context = context;
        this.values = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.adapter_item_list, parent, false);
        TextView nameView = rowView.findViewById(R.id.firstLine);
        TextView toBuyView = rowView.findViewById(R.id.secondLine);
        ImageView imageView = rowView.findViewById(R.id.icon);
        TextView itemAmount = rowView.findViewById(R.id.item_ammount);



        // Minus button view and onClickListener
        Button minusBtn = rowView.findViewById(R.id.minus_btn);
        minusBtn.setTag(values.get(position));
        minusBtn.setOnClickListener(v -> {
            ItemInPantryList item = (ItemInPantryList) v.getTag();
            int amount = item.decreaseAmount();

            // Disable
            if (amount == 0) {
                minusBtn.setEnabled(false);
            }

            itemAmount.setText(String.valueOf(amount));
            String toBuy = context.getString(R.string.to_buy) + " "+ item.getAmountShop();
            toBuyView.setText(toBuy);

            new Thread(() -> Database.updateItemAmount(item.getPantry().getId(), item.getItem().getName(), item.getAmountShop(), item.getAmountPantry())).start();

            try {
                RemoteOperations.updateItemAmount(item.getPantry().getId().toString(), item.getItem().getName(), item.getAmountShop(), amount);
            } catch (ServerUnreachableException e) {
                e.printStackTrace();
            }
        });

        nameView.setText(values.get(position).getItem().getName());
        String toBuy = context.getString(R.string.to_buy) + " " + values.get(position).getAmountShop();
        toBuyView.setText(toBuy);
        itemAmount.setText(String.valueOf(values.get(position).getAmountPantry()));
        if (values.get(position).getAmountPantry() == 0) {
            minusBtn.setEnabled(false);
        }
        imageView.setImageResource(R.mipmap.ic_launcher_foreground);


        return rowView;
    }

    public ItemInPantryList getAmount(int position) {
        return values.get(position);
    }
}
