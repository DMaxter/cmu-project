package pt.ulisboa.tecnico.cmu.server.dto;

import pt.ulisboa.tecnico.cmu.server.domain.Store;

import java.util.Collection;
import java.util.TreeMap;

public class ListStoresDto {
    private final TreeMap<String, CoordinatesDto> shops;

    public ListStoresDto(Collection<Store> stores) {
        this.shops = new TreeMap<>();

        for (Store s: stores) {
            this.shops.put(s.getName(), new CoordinatesDto(s.getLocation()));
        }
    }

    public TreeMap<String, CoordinatesDto> getShops() {
        return shops;
    }
}
