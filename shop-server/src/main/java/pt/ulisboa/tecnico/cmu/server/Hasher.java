package pt.ulisboa.tecnico.cmu.server;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {
    /**
     * Generate a hex string MD5 hash
     *
     * @param input - the string to be hashed
     * @return the MD5 hash
     */
    public static String hash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(StandardCharsets.UTF_8));

            StringBuilder builder = new StringBuilder();
            for(byte b: digest.digest()) {
                builder.append(String.format("%02x", b));
            }

            return builder.toString();
        } catch (NoSuchAlgorithmException e) {
            // Won't happen EVER
            return null;
        }
    }

    /**
     * Check if hash matches
     *
     * @param hash - the MD5 hash to check against
     * @param input - the string to be hashed
     * @return the result of comparing the generated hash to the given one
     */
    public static boolean verify(String hash, String input) {
        return hash(input).equals(hash);
    }
}