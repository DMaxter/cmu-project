package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidCoordinatesException extends Exception {
    public InvalidCoordinatesException() {
        super("INVALID COORDINATES");
    }
}
