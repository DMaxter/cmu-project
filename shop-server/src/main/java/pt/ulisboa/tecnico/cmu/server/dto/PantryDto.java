package pt.ulisboa.tecnico.cmu.server.dto;

import pt.ulisboa.tecnico.cmu.server.domain.AmountInfo;
import pt.ulisboa.tecnico.cmu.server.domain.PantryList;

import java.util.Map;
import java.util.TreeMap;

public class PantryDto {
    private final String id;
    private final String name;
    private final CoordinatesDto coords;
    private final TreeMap<String, AmountDto> items;

    public PantryDto(PantryList list) {
        this.id = list.getId().toString();
        this.name = list.getName();
        this.coords = new CoordinatesDto(list.getLocation());
        this.items = new TreeMap<>();

        for(Map.Entry<String, AmountInfo> i: list.getItems().entrySet()) {
            AmountInfo info = i.getValue();
            this.items.put(i.getKey(), new AmountDto(info.getAmountPantry(), info.getAmountShop()));
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CoordinatesDto getCoords() {
        return coords;
    }

    public TreeMap<String, AmountDto> getItems() {
        return items;
    }
}

