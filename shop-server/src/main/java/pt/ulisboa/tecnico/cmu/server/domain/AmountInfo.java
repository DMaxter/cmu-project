package pt.ulisboa.tecnico.cmu.server.domain;

public class AmountInfo {
    private final Item item;
    private int amountPantry;
    private int amountShop;

    public AmountInfo(Item item, int pantry, int shop) {
        this.item = item;
        this.amountPantry = pantry;
        this.amountShop = shop;
    }

    public Item getItem() {
        return this.item;
    }

    public int getAmountPantry() {
        return this.amountPantry;
    }

    public int getAmountShop() {
        return this.amountShop;
    }

    public void setAmountPantry(int amountPantry) {
        this.amountPantry = amountPantry;
    }

    public void setAmountShop(int amountShop) {
        this.amountShop = amountShop;
    }
}
