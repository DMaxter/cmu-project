package pt.ulisboa.tecnico.cmu.server.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class User implements java.io.Serializable {
    private String username;
    private String hash;
    private final HashMap<UUID, PantryList> ownedLists = new HashMap<>();
    private final HashMap<UUID, PantryList> sharedLists = new HashMap<>();

    /**
     * Create a new user
     *
     * @param username - the username of the user
     * @param hash - the hash of the password
     */
    public User(String username, String hash) {
        this.username = username;
        this.hash = hash;
    }

    public String getUsername() {
        return this.username;
    }

    public HashMap<UUID, PantryList> getSharedLists() {
        return this.sharedLists;
    }

    public HashMap<UUID, PantryList> getOwnedLists() {
        return this.ownedLists;
    }

    public List<PantryList> getAllLists() {
        Stream<PantryList> ownedStream = this.ownedLists.values().parallelStream();
        Stream<PantryList> sharedStream = this.sharedLists.values().parallelStream();

        Stream<PantryList> result = Stream.concat(ownedStream, sharedStream);

        return result.collect(Collectors.toList());
    }

    public void changeUsername(String username) {
        this.sharedLists.values().parallelStream().forEach(l -> {
            l.removeUser(this.username);
            l.addUser(username, this);
        });

        this.username = username;
    }

    public void changePassword(String hash) {
        this.hash = hash;
    }

    
    public boolean login(String hash) {
        return !this.hash.equals(hash);
    }

    public void updateOwnedList(UUID id, Item item, Integer pantryAmount, Integer shopAmount) {
        this.ownedLists.get(id).changeItemAmount(item, pantryAmount, shopAmount);
    }

    public void addOwnedList(UUID id, PantryList list) {
        this.ownedLists.put(id, list);
    }

    public void removeOwnedList(UUID id) {
        this.ownedLists.remove(id);
    }

    public void updateSharedList(UUID id, Item item, Integer pantryAmount, Integer shopAmount) {
        this.sharedLists.get(id).changeItemAmount(item, pantryAmount, shopAmount);
    }

    public void addSharedList(UUID id, PantryList list) {
        this.sharedLists.put(id, list);
        list.addUser(this.username, this);
    }

    public void removeSharedList(UUID id) {
        this.sharedLists.remove(id).removeUser(this.username);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
