package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidRegisterException extends Exception {
    public InvalidRegisterException() {
        super("INVALID REGISTER");
    }
}
