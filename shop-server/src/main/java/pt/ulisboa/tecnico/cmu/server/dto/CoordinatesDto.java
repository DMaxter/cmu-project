package pt.ulisboa.tecnico.cmu.server.dto;

import pt.ulisboa.tecnico.cmu.server.domain.Coordinates;

public class CoordinatesDto {
    private final double latitude;
    private final double longitude;

    public CoordinatesDto(Coordinates coords) {
        this.latitude = coords.getLatitude();
        this.longitude = coords.getLongitude();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
