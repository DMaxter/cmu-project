package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidListException extends Exception {
    public InvalidListException() {
        super("INVALID LIST");
    }
}
