package pt.ulisboa.tecnico.cmu.server.domain;

public interface ItemList {
    Coordinates getLocation();
}
