package pt.ulisboa.tecnico.cmu.server.domain;

public class UserQueueInfo {
    private final int items; // Total number of items (user's + users ahead of him)
    private final long time;

    public UserQueueInfo(int items) {
        this.items = items;
        this.time = System.currentTimeMillis();
    }

    public int getItems() {
        return items;
    }

    public long getTime() {
        return time;
    }
}
