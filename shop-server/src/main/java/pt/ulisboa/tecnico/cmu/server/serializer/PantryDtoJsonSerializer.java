package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.AmountDto;
import pt.ulisboa.tecnico.cmu.server.dto.CoordinatesDto;
import pt.ulisboa.tecnico.cmu.server.dto.PantryDto;

import java.lang.reflect.Type;

public class PantryDtoJsonSerializer implements JsonSerializer<PantryDto> {
    @Override
    public JsonElement serialize(PantryDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("id", context.serialize(dto.getId()));
        obj.add("name", context.serialize(dto.getName()));
        obj.add("coords", context.serialize(dto.getCoords()));
        obj.add("items", context.serialize(dto.getItems()));

        return obj;
    }

    public static String serialize(PantryDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(PantryDto.class, new PantryDtoJsonSerializer())
                .registerTypeAdapter(AmountDto.class, new AmountDtoJsonSerializer())
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}