package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.CoordinatesDto;
import pt.ulisboa.tecnico.cmu.server.dto.ListStoresDto;

import java.lang.reflect.Type;

public class ListStoresDtoJsonSerializer implements JsonSerializer<ListStoresDto> {
    @Override
    public JsonElement serialize(ListStoresDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("shops", context.serialize(dto.getShops()));

        return obj;
    }

    public static String serialize(ListStoresDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(ListStoresDto.class, new ListStoresDtoJsonSerializer())
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
