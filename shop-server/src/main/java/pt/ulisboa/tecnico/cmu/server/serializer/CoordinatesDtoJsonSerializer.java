package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.CoordinatesDto;

import java.lang.reflect.Type;

public class CoordinatesDtoJsonSerializer implements JsonSerializer<CoordinatesDto> {
    @Override
    public JsonElement serialize(CoordinatesDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("latitude", context.serialize(dto.getLatitude()));
        obj.add("longitude", context.serialize(dto.getLongitude()));

        return obj;
    }

    public static String serialize(CoordinatesDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(CoordinatesDto.class, new CoordinatesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
