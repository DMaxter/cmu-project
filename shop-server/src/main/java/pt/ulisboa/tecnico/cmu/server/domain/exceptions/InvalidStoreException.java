package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidStoreException extends Exception {
    public InvalidStoreException() {
        super("INVALID STORE");
    }
}
