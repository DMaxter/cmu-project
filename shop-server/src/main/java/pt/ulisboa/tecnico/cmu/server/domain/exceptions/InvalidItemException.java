package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidItemException extends Exception {
    public InvalidItemException() {
        super("INVALID ITEM");
    }
}
