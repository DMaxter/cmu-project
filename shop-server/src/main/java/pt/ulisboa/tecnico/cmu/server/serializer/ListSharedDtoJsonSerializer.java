package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.ListSharedDto;

import java.lang.reflect.Type;

public class ListSharedDtoJsonSerializer implements JsonSerializer<ListSharedDto> {
    @Override
    public JsonElement serialize(ListSharedDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("users", context.serialize(dto.getUsers()));

        return obj;
    }

    public static String serialize(ListSharedDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(ListSharedDto.class, new ListSharedDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
