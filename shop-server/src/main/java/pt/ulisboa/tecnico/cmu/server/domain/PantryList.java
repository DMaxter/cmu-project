package pt.ulisboa.tecnico.cmu.server.domain;


import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidLocationException;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidUserException;

import java.util.HashMap;
import java.util.UUID;

public class PantryList implements ItemList, java.io.Serializable {
    private final UUID id;
    private String name;
    private Coordinates coords;
    private User owner;
    private final HashMap<String, User> allowed = new HashMap<>();
    private final HashMap<String, AmountInfo> items = new HashMap<>();

    public PantryList(UUID id, String name, Coordinates coords, User owner) throws InvalidLocationException, InvalidUserException {
        if (coords == null) {
            throw new InvalidLocationException();
        } else if (owner == null) {
            throw new InvalidUserException();
        }

        this.id = id;
        this.name = name;
        this.coords = coords;
        this.owner = owner;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinates getLocation() {
        return this.coords;
    }

    public void setLocation(Coordinates coords) {
        this.coords = coords;
    }

    public User getOwner() {
        return this.owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public HashMap<String, User> getAllowed() {
        return this.allowed;
    }

    public void addUser(String username, User user) {
        this.allowed.put(username, user);
    }

    public void removeUser(String user) {
        this.allowed.remove(user);
    }

    public HashMap<String, AmountInfo> getItems() {
        return items;
    }

    public void changeItemAmount(Item item, Integer pantryAmount, Integer shopAmount) {
        AmountInfo info;
        if (shopAmount == 0 && pantryAmount == 0) {
            items.remove(item.getName());
        } else if ((info = items.get(item.getName())) == null) {
            info = new AmountInfo(item, pantryAmount, shopAmount);
            items.put(item.getName(), info);
        } else {
            if (pantryAmount != null) {
                info.setAmountPantry(pantryAmount);
            }

            info.setAmountShop(shopAmount);
        }
    }

    public void removeItem(String item) {
        items.remove(item);
    }

    public int buy(String item, int amount) {
        if (!items.containsKey(item) || items.get(item).getAmountShop() == 0) {
            return 0;
        }

        AmountInfo info = items.get(item);

        if (info.getAmountShop() > - amount) {
            info.setAmountShop(info.getAmountShop() + amount);
            info.setAmountPantry(info.getAmountPantry() - amount);
            return 0;
        }
        else {
            int amountBought = info.getAmountShop();

            info.setAmountShop(0);
            info.setAmountPantry(info.getAmountPantry() + amountBought);

            return amount + amountBought;
        }
    }
}
