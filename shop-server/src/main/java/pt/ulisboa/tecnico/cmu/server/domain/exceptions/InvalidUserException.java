package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidUserException extends Exception {
    public InvalidUserException() {
        super("INVALID USER");
    }
}
