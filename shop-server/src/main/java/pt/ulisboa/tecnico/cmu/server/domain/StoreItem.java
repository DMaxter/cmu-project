package pt.ulisboa.tecnico.cmu.server.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class StoreItem implements Comparable<StoreItem> {
    private String name;
    private double price;
    private HashMap<String, Integer> firstThanCounts = new HashMap<>();
    private HashMap<String, Integer> boughtTogetherCounts = new HashMap<>();

    public StoreItem(String name, double price, Collection<String> items) {
        this.name = name;
        this.price = price;

        for (String item: items) {
            firstThanCounts.put(item, 0);
            boughtTogetherCounts.put(item, 0);
        }
    }

    public String getName() {
        return name;
    }

    public void addOtherStoreItem(String item) {
        firstThanCounts.put(item, 0);
        boughtTogetherCounts.put(item, 0);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void firstThan(String item) {
        firstThanCounts.put(item, firstThanCounts.get(item) + 1);
    }

    public void boughtTogether(String item) {
        boughtTogetherCounts.put(item, boughtTogetherCounts.get(item) + 1);
    }

    public int getFirstCount(String item) {
        return firstThanCounts.get(item);
    }

    public String frequentlyBoughtTogether() {
        String frequentlyBoughtTogether = "";
        Integer value = 0;
        for (Map.Entry<String, Integer> entry: boughtTogetherCounts.entrySet()) {
            if (entry.getValue() >= value) {
                frequentlyBoughtTogether = entry.getKey();
                value = entry.getValue();
            }
        }
        return frequentlyBoughtTogether;
    }

    @Override
    public int compareTo(StoreItem o) {
        if (o == this) {
            return 0;
        }

        if (getFirstCount(o.getName()) > o.getFirstCount(name)) {
            return -1;
        }
        else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "StoreItem name: " + name + " price: " + price + " firstCounts: " + firstThanCounts + " togetherCounts" + boughtTogetherCounts;
    }
}
