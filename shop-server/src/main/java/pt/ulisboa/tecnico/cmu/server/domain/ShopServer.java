package pt.ulisboa.tecnico.cmu.server.domain;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ShopServer {

    private final HashMap<String, Item> items = new HashMap<>();
    private final HashMap<String, Store> stores = new HashMap<>();
    private final HashMap<String, User> users = new HashMap<>();
    private final HashMap<UUID, PantryList> lists = new HashMap<>();
    private final Map<String, Map<String, UserQueueInfo>> queues = new ConcurrentHashMap<>();
    private final Map<String, SimpleRegression> regressions = new ConcurrentHashMap<>();

    public ShopServer() {
    }

    //
    // Login/Register-related functions
    //

    /**
     * Register a user
     *
     * @param username - the username of the user trying to register
     * @param hash - the hash of the password of the user
     * @param guid - the guid of the user
     * @throws InvalidRegisterException if a user with the given username is already registered
     */
    public void register(String username, String hash, String guid) throws InvalidRegisterException {

        if (users.containsKey(username)) {
            throw new InvalidRegisterException();
        }
        else if (users.containsKey(guid)) {
            // Convert a GUID user to a Named User
            User guest = users.get(guid);
            guest.changeUsername(username);
            guest.changePassword(hash);
            users.put(username, guest);
            users.remove(guid);
        }
        else {
            users.put(username, new User(username, hash));
        }
    }

    /**
     * Login user
     *
     * @param username - the username of the user trying to login
     * @param hash - the hash of the password of the user
     * @throws InvalidLoginException if username doesn't exist or hash doesn't match
     */
    public void login(String username, String hash) throws InvalidLoginException {

        User user = users.get(username);
        if (user == null || user.login(hash)) {
            throw new InvalidLoginException();
        }
    }

    /**
     * Login/Register guest
     *
     * @param guid - the guid of the guest trying to login/register
     * @param hash - the hash of the guid
     * @throws InvalidLoginException if hash doesn't match
     */
    public void guest(String guid, String hash) throws InvalidLoginException {

        User user = users.get(guid);
        if (user == null) {
            user = new User(guid, hash);
            users.put(guid, user);
        }
        else if (user.login(hash)) {
            throw new InvalidLoginException();
        }
    }



    //
    // List-related functions
    //

    /**
     * Create a Pantry List
     *
     * @param username - the username of the user creating the list
     * @param list_name - the name of the pantry list
     * @param latitude - the latitude (coordinates of the list)
     * @param longitude - the longitude (coordinates of the list)
     */
    public void createList(String list_id, String username, String list_name, double latitude, double longitude) throws InvalidCoordinatesException, InvalidLocationException, InvalidUserException {

        User user = users.get(username);
        Coordinates coords = new Coordinates(latitude, longitude);
        UUID id = UUID.fromString(list_id);
        PantryList list = new PantryList(id, list_name, coords, user);

        lists.put(id, list);
        user.addOwnedList(id, list);
    }

    /**
     * Edit a Pantry List
     *
     * @param list_id - the id of the list being edited
     * @param owner_name - username of the owner of the list
     * @param list_name - the new name of the pantry list
     * @param latitude - the new latitude (coordinates of the list)
     * @param longitude - the new longitude (coordinates of the list)
     */
    public void editList(String list_id, String owner_name, String list_name, double latitude, double longitude) throws InvalidCoordinatesException, InvalidListException, UserNotOwnerException {

        PantryList list = lists.get(UUID.fromString(list_id));

        if (list == null) {
            throw new InvalidListException();
        }

        if (!list.getOwner().getUsername().equals(owner_name)) {
            throw new UserNotOwnerException();
        }

        list.setLocation(new Coordinates(latitude, longitude));
        list.setName(list_name);
    }

    /**
     * Delete a Pantry List
     *
     * @param list_id - the id of the pantry list
     * @param username - the username of the user creating the list
     * @throws UserNotOwnerException if the user is not the list owner
     */
    public void deleteList(String list_id, String username) throws UserNotOwnerException {

        User user = users.get(username);
        UUID id = UUID.fromString(list_id);

        if (!user.getOwnedLists().containsKey(id)) {
            throw new UserNotOwnerException();
        }

        PantryList list = lists.get(id);

        HashMap<String, User> users_to_remove = list.getAllowed();

        for (User u: users_to_remove.values()) {
            u.removeSharedList(id);
        }

        user.removeOwnedList(id);
        lists.remove(id);
    }

    /**
     * Get all lists from user specified by username
     *
     * @param username - the username of a user
     * @return the metadata of the lists belonging to that user
     */
    public List<PantryList> getLists(String username) {
        return users.get(username).getAllLists();
    }


    /**
     * Get shared users of a specific list
     *
     * @param list_id - the id of the pantry list
     * @param owner_name - the username of list owner
     * @throws UserNotOwnerException if the user is not the list owner
     * @return the names of the users who have access to the list
     */
    public Set<String> getSharedUsers(String list_id, String owner_name) throws UserNotOwnerException, InvalidListException {

        PantryList list = lists.get(UUID.fromString(list_id));

        if (list == null) {
            throw new InvalidListException();
        }

        if (!list.getOwner().getUsername().equals(owner_name)) {
            throw new UserNotOwnerException();
        }

        return list.getAllowed().keySet();
    }

    /**
     * Get a specific list
     *
     * @param list_id - the if of the list
     * @return the list and its contents
     */
    public PantryList getList(String list_id) {
        UUID id = UUID.fromString(list_id);
        return lists.get(id);
    }

    /**
     * Share a list with other user
     *
     * @param user_sharing - user that is sharing the list
     * @param user_to_share - user that will be allowed to see the list
     * @param list - the id of the list that will be shared
     * @throws InvalidUserException if one of the users is invalid
     * @throws InvalidListException if the list doesn't exist
     * @throws UserNotOwnerException if the user sharing is not the owner of the list
     */
    public void shareList(String user_sharing, String user_to_share, String list) throws InvalidUserException, InvalidListException, UserNotOwnerException {
        if (!users.containsKey(user_sharing) || !users.containsKey(user_to_share)) {
            throw new InvalidUserException();
        }

        PantryList sharedList = lists.get(UUID.fromString(list));
        if (sharedList == null) {
            throw new InvalidListException();
        }

        User owner = this.users.get(user_sharing);

        if(sharedList.getOwner() != owner) {
            throw new UserNotOwnerException();
        }

        this.users.get(user_to_share).addSharedList(sharedList.getId(), sharedList);
    }

    /**
     * Unshare a list with other user
     *
     * @param user_unsharing - the user that is unsharing the list
     * @param user_to_unshare - the user that will not be able to see the list
     * @param list - the id of the list that is going to be unshared
     * @throws InvalidUserException if one of the users is invalid
     * @throws InvalidListException if the list doesn't exist
     * @throws UserNotOwnerException if the user unsharing is not the owner of the list
     */
    public void unshareList(String user_unsharing, String user_to_unshare, String list) throws InvalidUserException, InvalidListException, UserNotOwnerException {
        if (!users.containsKey(user_unsharing) || !users.containsKey(user_to_unshare)) {
            throw new InvalidUserException();
        }

        PantryList sharedList = lists.get(UUID.fromString(list));
        if (sharedList == null) {
            throw new InvalidListException();
        }

        User owner = this.users.get(user_unsharing);

        if(sharedList.getOwner() != owner) {
            throw new UserNotOwnerException();
        }

        this.users.get(user_to_unshare).removeSharedList(sharedList.getId());
    }

    /**
     * Remove an item from a pantry list
     *
     * @param username - the username of the user trying to remove the item
     * @param list_id - the id of the list to remove the item from
     * @param item - the name of the item to be removed
     * @throws InvalidUserException if the user doesn't exist
     * @throws InvalidListException if the list doesn't exist
     * @throws UserNotOwnerException if the user is not the owner of the list
     * @throws InvalidItemException if the item doesn't exist
     */
    public void removeItemFromList(String username, String list_id, String item) throws InvalidUserException, InvalidListException, UserNotOwnerException, InvalidItemException {
        User user = this.users.get(username);
        if (user == null) {
            throw new InvalidUserException();
        }

        PantryList list = this.lists.get(UUID.fromString(list_id));
        if (list == null) {
            throw new InvalidListException();
        }

        if (list.getOwner() != user) {
            throw new UserNotOwnerException();
        }

        if (!items.containsKey(item)) {
            throw new InvalidItemException();
        }

        list.removeItem(item);
    }



    //
    // Item-related functions
    //

    /**
     * Creates a new item
     *
     * @param item_name - the name of the new item
     * @param barcode - the optional item barcode
     * @throws InvalidItemException if the item already exists
     */
    public void createItem(String item_name, String barcode) throws InvalidItemException {

        if (items.containsKey(item_name) && !items.get(item_name).getBarcode().isBlank() && !items.get(item_name).getBarcode().equals(barcode)) {
            throw new InvalidItemException();
        }
        else if (!items.containsKey(item_name)) {
            items.put(item_name, new Item(item_name, barcode));
        }
    }

    /**
     * Add a picture to an item
     *
     * @param item_name - the name of the item
     * @param picture - the picture to be added
     * @throws InvalidItemException if item does not exist
     */
    public void addItemPicture(String item_name, String picture) throws InvalidItemException {

        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        item.addPicture(picture);
    }

    /**
     * Add a new item to a store and its price
     *
     * @param item_name - the name of the item
     * @param store_name - the name of the new store
     * @param price - the price of the item in that store
     * @throws InvalidItemException if item does not exist
     * @throws InvalidStoreException if store does not exist
     */
    public void addItemStore(String item_name, String store_name, double price) throws InvalidItemException, InvalidStoreException {

        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        Store store = stores.get(store_name);
        if (store == null) {
            throw new InvalidStoreException();
        }

        store.addItem(item_name, price);
    }

    /**
     * Changes the amount of an item in a specific pantry list
     *
     * @param list_id - the id of the list
     * @param item_name - the name of the item
     * @param pantryAmount - the new amount of the item for that list in the pantry
     * @param shopAmount - the new amount of the item for that list to be bought
     * @throws InvalidListException if list does not exist
     * @throws InvalidItemException if item does not exist
     */
    public void updateItemAmount(String list_id, String item_name, Integer pantryAmount, Integer shopAmount) throws InvalidListException, InvalidItemException {

        UUID id = UUID.fromString(list_id);
        PantryList list = lists.get(id);
        if (list == null) {
            throw new InvalidListException();
        }

        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        list.changeItemAmount(item, pantryAmount, shopAmount);

    }

    /**
     * Changes the details of an item
     *
     * @param list_id - the id of the list
     * @param old_item_name - the old name of the item
     * @param new_item_name - the new name of the item
     * @param barcode - the new name of the item
     * @param pantryAmount - the new amount of the item for that list in the pantry
     * @param shopAmount - the new amount of the item for that list to be bought
     * @throws InvalidListException if list does not exist
     * @throws InvalidItemException if item does not exist
     */
    public void updateItemDetails(String list_id, String old_item_name, String new_item_name, String barcode, Integer pantryAmount, Integer shopAmount) throws InvalidListException, InvalidItemException {

        UUID id = UUID.fromString(list_id);
        PantryList list = lists.get(id);
        if (list == null) {
            throw new InvalidListException();
        }

        Item item = items.get(old_item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        if (items.containsKey(new_item_name) && !new_item_name.equals(old_item_name)) {
            list.removeItem(old_item_name);
        }
        else {
            item.setName(new_item_name);
            item.setBarcode(barcode);

            items.remove(old_item_name);
            items.put(new_item_name, item);
        }

        list.changeItemAmount(items.get(new_item_name), pantryAmount, shopAmount);
    }

    public void rateItem(String username, String item_name, int rating) throws InvalidItemException {
        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        item.rateItem(username, rating);
    }

    public Boolean canRate(String username, String item_name) throws InvalidItemException {
        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        return item.canRateItem(username);
    }

    public Collection<Integer> getItemRating(String item_name) throws InvalidItemException {
        Item item = items.get(item_name);
        if (item == null) {
            throw new InvalidItemException();
        }

        return item.getRatings();
    }

    public void createStore(String name, double latitude, double longitude) throws InvalidStoreException, InvalidLocationException, InvalidCoordinatesException {

        if (stores.containsKey(name)) {
            throw new InvalidStoreException();
        }

        Coordinates coordinates = new Coordinates(latitude, longitude);
        stores.put(name, new Store(name, coordinates));
        queues.put(name, new ConcurrentHashMap<>());
        regressions.put(name, new SimpleRegression());
    }

    public Map<Store, Long> getStores(String username) throws InvalidUserException {
        if (users.get(username) == null) {
            throw new InvalidUserException();
        }

        return stores.entrySet().stream().collect(Collectors.toMap(Map.Entry<String, Store>::getValue, e -> {
            try {
                SimpleRegression regression = regressions.get(e.getKey());
                double a = regression.getSlope();

                if (Double.isNaN(a)) {
                    return -1L;
                }

                return ((Double) (a *
                        (getStoreView(username, e.getKey()).stream().mapToInt(i -> getAllShopAmount(username, i.getName())).sum() + queues.get(e.getKey()).values()
                                .stream()
                                .mapToInt(UserQueueInfo::getItems)
                                .max().orElse(0)))).longValue();
            } catch (InvalidStoreException | InvalidUserException invalidStoreException) {
                // Ignore
                return -1L;
            }
        }));
    }

    public Set<StoreItem> getStoreView(String username, String store_name) throws InvalidUserException, InvalidStoreException {

        User user = users.get(username);
        if (user == null) {
            throw new InvalidUserException();
        }

        Store store = stores.get(store_name);
        if (store == null) {
            throw  new InvalidStoreException();
        }


        Set<String> items_in_pantries = new HashSet<String>();
        for (PantryList list: user.getAllLists()) {
            items_in_pantries.addAll(list.getItems().keySet());
        }

        return store.getPantryItemsInStore(items_in_pantries);
    }

    public int getAllShopAmount(String username, String item_name) {

        User user = users.get(username);
        List<PantryList> lists = user.getAllLists();
        int totalShopAmount = 0;

        for (PantryList list: lists) {
            if (list.getItems().containsKey(item_name)) {
                totalShopAmount += list.getItems().get(item_name).getAmountShop();
            }
        }

        return totalShopAmount;
    }

    public Collection<Item> getAllItems() {
        return items.values();
    }

    public Item getItem(String item_name) {
        return items.get(item_name);
    }

    public void buyItem(String username, String item_name, int shopAmount) {
        User user = users.get(username);
        List<PantryList> pantryLists = user.getAllLists();

        for (PantryList p: pantryLists) {
            shopAmount += p.buy(item_name, shopAmount);
            if (shopAmount == 0) {
                break;
            }
        }
    }

    public void updateStore(String store_name, String item_name_1, String item_name_2) {
        Store store = stores.get(store_name);
        StoreItem item = store.getItem(item_name_1);
        item.firstThan(item_name_2);
        item.boughtTogether(item_name_2);
        store.getItem(item_name_2).boughtTogether(item_name_1);
    }

    public Collection<String> getItemsNotInStore(String store_name) throws InvalidStoreException {
        Store store = stores.get(store_name);
        if (store == null) {
            throw new InvalidStoreException();
        }

        return store.getItemsNotInStore(items.keySet());
    }

    public void updateItemPrice(String store_name, String item_name, double price) throws InvalidStoreException {
        Store store = stores.get(store_name);
        if (store == null) {
            throw new InvalidStoreException();
        }

        store.updateItemPrice(item_name, price);
    }

    public void addUserToQueue(String store, String user, int items) {
        Map<String, UserQueueInfo> storeQueue = queues.get(store);

        storeQueue.remove(user);

        int itemsInQueue = storeQueue.values().stream().mapToInt(UserQueueInfo::getItems).max().orElse(0);
        storeQueue.put(user, new UserQueueInfo(items + itemsInQueue));
    }

    public synchronized void removeUserFromQueue(String store, String user) {
        UserQueueInfo info = queues.get(store).remove(user);
        long elapsed = System.currentTimeMillis() - info.getTime();

        regressions.get(store).addData(info.getItems(), elapsed);
    }
}
