package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidLoginException extends Exception {
    public InvalidLoginException() {
        super("INVALID LOGIN");
    }
}
