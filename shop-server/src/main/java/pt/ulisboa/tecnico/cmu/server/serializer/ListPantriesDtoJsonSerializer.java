package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.ListPantriesDto;

import java.lang.reflect.Type;

public class ListPantriesDtoJsonSerializer implements JsonSerializer<ListPantriesDto> {
    @Override
    public JsonElement serialize(ListPantriesDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("pantries", context.serialize(dto.getPantries()));

        return obj;
    }

    public static String serialize(ListPantriesDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(ListPantriesDto.class, new ListPantriesDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
