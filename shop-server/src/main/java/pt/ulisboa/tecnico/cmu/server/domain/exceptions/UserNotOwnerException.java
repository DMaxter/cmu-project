package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class UserNotOwnerException extends Exception {
    public UserNotOwnerException() {
        super("USER NOT OWNER");
    }
}
