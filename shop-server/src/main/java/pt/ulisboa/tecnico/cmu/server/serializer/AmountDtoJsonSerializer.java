package pt.ulisboa.tecnico.cmu.server.serializer;

import com.google.gson.*;
import pt.ulisboa.tecnico.cmu.server.dto.AmountDto;

import java.lang.reflect.Type;

public class AmountDtoJsonSerializer implements JsonSerializer<AmountDto> {
    @Override
    public JsonElement serialize(AmountDto dto, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();

        obj.add("pantry", context.serialize(dto.getAmountPantry()));
        obj.add("shop", context.serialize(dto.getAmountShop()));

        return obj;
    }

    public static String serialize(AmountDto o) {
        return new GsonBuilder()
                .registerTypeAdapter(AmountDto.class, new AmountDtoJsonSerializer())
                .create()
                .toJson(o);
    }
}
