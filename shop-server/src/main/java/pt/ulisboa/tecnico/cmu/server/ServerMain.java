package pt.ulisboa.tecnico.cmu.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.File;
import java.io.IOException;

public class ServerMain {
    private static final String certChain = "/etc/letsencrypt/live/shopist.gq/fullchain.pem";
    private static final String privKey = "/etc/letsencrypt/live/shopist.gq/privkey.pem";
    public static final int port = 1337;

    public static void main(String[] args) throws InterruptedException {
        System.out.println(ServerMain.class.getSimpleName());

        Server server = ServerBuilder.forPort(port)
                .useTransportSecurity(new File(certChain), new File(privKey))
                .addService(new ShopService())
                .build();

        System.out.println("Server listening on port " + port);
        try {
            server.start();
            server.awaitTermination();
        } catch (IOException e) {
            System.err.println("Could not bind to port");
        }

    }
}
