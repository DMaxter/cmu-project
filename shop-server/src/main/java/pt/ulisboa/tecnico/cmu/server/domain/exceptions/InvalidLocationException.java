package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidLocationException extends Exception {
    public InvalidLocationException() {
        super("INVALID LOCATION");
    }
}
