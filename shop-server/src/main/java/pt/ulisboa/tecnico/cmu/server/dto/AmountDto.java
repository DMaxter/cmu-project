package pt.ulisboa.tecnico.cmu.server.dto;

public class AmountDto {
    private int amountPantry;
    private int amountShop;

    public AmountDto(int pantry, int shop) {
        this.amountPantry = pantry;
        this.amountShop = shop;
    }

    public int getAmountPantry() {
        return this.amountPantry;
    }

    public int getAmountShop() {
        return this.amountShop;
    }

    public void setAmountPantry(int amountPantry) {
        this.amountPantry = amountPantry;
    }

    public void setAmountShop(int amountShop) {
        this.amountShop = amountShop;
    }
}
