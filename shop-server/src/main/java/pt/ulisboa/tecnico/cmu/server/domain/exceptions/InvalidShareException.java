package pt.ulisboa.tecnico.cmu.server.domain.exceptions;

public class InvalidShareException extends Exception {
    public InvalidShareException() {
        super("INVALID SHARE");
    }
}
