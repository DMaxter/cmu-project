package pt.ulisboa.tecnico.cmu.server;

import io.grpc.stub.StreamObserver;
import pt.ulisboa.tecnico.cmu.grpc.Shop;
import pt.ulisboa.tecnico.cmu.grpc.Shop.ItemList;
import pt.ulisboa.tecnico.cmu.grpc.Shop.*;
import pt.ulisboa.tecnico.cmu.grpc.ShopServiceGrpc;
import pt.ulisboa.tecnico.cmu.server.domain.Item;
import pt.ulisboa.tecnico.cmu.server.domain.Store;
import pt.ulisboa.tecnico.cmu.server.domain.*;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidItemException;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidListException;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidUserException;
import pt.ulisboa.tecnico.cmu.server.domain.exceptions.UserNotOwnerException;
import pt.ulisboa.tecnico.cmu.server.dto.ListPantriesDto;
import pt.ulisboa.tecnico.cmu.server.dto.ListSharedDto;
import pt.ulisboa.tecnico.cmu.server.dto.ListStoresDto;
import pt.ulisboa.tecnico.cmu.server.dto.PantryDto;
import pt.ulisboa.tecnico.cmu.server.serializer.ListPantriesDtoJsonSerializer;
import pt.ulisboa.tecnico.cmu.server.serializer.ListSharedDtoJsonSerializer;
import pt.ulisboa.tecnico.cmu.server.serializer.ListStoresDtoJsonSerializer;
import pt.ulisboa.tecnico.cmu.server.serializer.PantryDtoJsonSerializer;

import java.util.*;
import java.util.logging.Logger;

import static io.grpc.Status.FAILED_PRECONDITION;
import static io.grpc.Status.INVALID_ARGUMENT;

public class ShopService extends ShopServiceGrpc.ShopServiceImplBase {

    private static final Logger LOGGER = Logger.getLogger(ShopService.class.getName());
    private final ShopServer server;

    public ShopService() {
        server = new ShopServer();
    }

    //
    // Login/Register-related services
    //
    @Override
    public void register(RegisterRequest request, StreamObserver<RegisterResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [REGISTER] [STARTED]");

        try {
            server.register(request.getUsername(), request.getHash(), request.getGuid());

            responseObserver.onNext(RegisterResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [REGISTER] [FINISHED] [ADDED USER: " + request.getUsername() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [REGISTER] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void login(LoginRequest request, StreamObserver<LoginResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [LOGIN] [STARTED]");

        try {
            server.login(request.getUsername(), request.getHash());

            responseObserver.onNext(LoginResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [LOGIN] [FINISHED] [USER: " + request.getUsername() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [LOGIN] [ERROR]");
            e.printStackTrace();
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void guest(GuestRequest request, StreamObserver<GuestResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [GUEST] [STARTED]");

        try {
            server.guest(request.getGuid(), request.getHash());

            responseObserver.onNext(GuestResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GUEST] [FINISHED] [GUID: " + request.getGuid() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GUEST] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    //
    // List-related services
    //
    @Override
    public void createList(CreateListRequest request, StreamObserver<CreateListResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_LIST] [STARTED]");

        try {
            server.createList(request.getId().getValue(), request.getUsername(), request.getListName(), request.getLatitude(), request.getLongitude());
            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_LIST] [FINISHED] [USER: " + request.getUsername() + " | LIST: " + request.getListName() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_LIST] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void editList(EditListRequest request, StreamObserver<EditListResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [EDIT_LIST] [STARTED]");

        try {
            server.editList(request.getListId().getValue(), request.getOwner(), request.getListName(), request.getLatitude(), request.getLongitude());

            responseObserver.onNext(EditListResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [EDIT_LIST] [FINISHED] [LIST_ID: " + request.getListId().getValue() + " | NAME: "
                    + request.getListName() + " | COORDINATES: (" + request.getLatitude() + " | " + request.getLongitude() + ")]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [EDIT_LIST] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void deleteList(DeleteListRequest request, StreamObserver<DeleteListResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [DELETE_LIST] [STARTED]");

        try {
            server.deleteList(request.getListId().getValue(), request.getUsername());

            responseObserver.onNext(DeleteListResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [DELETE_LIST] [FINISHED] [LIST_ID: " + request.getListId().getValue() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [DELETE_LIST] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getLists(GetListsRequest request, StreamObserver<GetListsResponse> responseObserver) {
        LOGGER.info("[STARTED] GetLists");

        List<PantryList> lists = server.getLists(request.getUsername());

        if (Hasher.verify(request.getHash(), ListPantriesDtoJsonSerializer.serialize(new ListPantriesDto(lists)))) {
            responseObserver.onNext(GetListsResponse.getDefaultInstance());
            responseObserver.onCompleted();

            LOGGER.info("[FINISHED] [CACHED] GetLists [User: " + request.getUsername() + "]");
            return;
        }

        GetListsResponse.Response.Builder response = GetListsResponse.Response.newBuilder();
        //Iterates through user pantry lists to create respective DTOs
        for (PantryList list : lists) {
            String id = list.getId().toString();
            String name = list.getName();
            String owner = list.getOwner().getUsername();
            double latitude = list.getLocation().getLatitude();
            double longitude = list.getLocation().getLongitude();

            response.addList(ItemList.newBuilder()
                    .setId(Shop.UUID.newBuilder().setValue(id).build())
                    .setName(name)
                    .setOwner(owner)
                    .setLatitude(latitude)
                    .setLongitude(longitude)
                    .build());
        }

        responseObserver.onNext(GetListsResponse.newBuilder().setResult(response.build()).build());
        responseObserver.onCompleted();

        LOGGER.info("[FINISHED] GetLists [User: " + request.getUsername() + "]");
    }

    @Override
    public void getSharedUsers(GetSharedUsersRequest request, StreamObserver<GetSharedUsersResponse> responseObserver) {
        LOGGER.info("[STARTED] GetSharedUsers");

        try {
            Set<String> sharedUsers = server.getSharedUsers(request.getListId().getValue(), request.getOwner());

            if (Hasher.verify(request.getHash(), ListSharedDtoJsonSerializer.serialize(new ListSharedDto(sharedUsers)))) {
                responseObserver.onNext(GetSharedUsersResponse.getDefaultInstance());
                responseObserver.onCompleted();

                LOGGER.info("[FINISHED] [CACHED] GetSharedUsers [ListID: " + request.getListId().getValue() + " | Owner: " + request.getOwner() + "]");
                return;
            }

            responseObserver.onNext(GetSharedUsersResponse.newBuilder()
                    .setResult(GetSharedUsersResponse.Response.newBuilder()
                            .addAllUsername(sharedUsers)
                            .build())
                    .build());
            responseObserver.onCompleted();

            LOGGER.info("[FINISHED] GetSharedUsers [ListID: " + request.getListId().getValue() + " | Owner: " + request.getOwner() + "]");
        } catch (Exception e) {
            LOGGER.info("[ERROR] GetSharedUsers - " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getList(GetListRequest request, StreamObserver<GetListResponse> responseObserver) {
        LOGGER.info("[STARTED] GetList");

        // Assume the list always exists
        PantryList list = server.getList(request.getListId().getValue());

        // Check if information didn't change
        if (Hasher.verify(request.getHash(), PantryDtoJsonSerializer.serialize(new PantryDto(list)))) {
            responseObserver.onNext(GetListResponse.getDefaultInstance());
            responseObserver.onCompleted();

            LOGGER.info("[FINISHED] [CACHED] GetList [List: " + list.getName() + "]");
            return;
        }

        GetListResponse.Response.Builder response = GetListResponse.Response.newBuilder();

        // Iterates through all items in the pantry list to create respective DTOs
        for (Map.Entry<String, AmountInfo> item_entry : list.getItems().entrySet()) {
            Item item = server.getItem(item_entry.getKey());
            String name = item.getName();
            String barcode = item.getBarcode();
            List<String> pictures = item.getPictures();
            AmountInfo info = item_entry.getValue();

            response.addItem(Shop.Item.newBuilder()
                    .setName(name)
                    .setBarcode(barcode)
                    .setPantryAmount(info.getAmountPantry())
                    .setShopAmount(info.getAmountShop())
                    .addAllPictures(pictures)
                    .build());
        }

        responseObserver.onNext(GetListResponse.newBuilder().setResult(response.build()).build());
        responseObserver.onCompleted();

        LOGGER.info("[FINISHED] GetList [List: " + list.getName() + "]");
    }

    @Override
    public void shareList(ShareListRequest request, StreamObserver<ShareListResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [SHARE_LIST] [STARTED]");

        try {
            server.shareList(request.getUsernameSharing(), request.getUsernameToShare(), request.getListId().getValue());

            responseObserver.onNext(ShareListResponse.getDefaultInstance());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [SHARE_LIST] [FINISHED]");
        } catch (InvalidUserException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("User is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [SHARE_LIST] [ERROR] " + e.getMessage());
        } catch (InvalidListException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("List is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [SHARE_LIST] [ERROR] " + e.getMessage());
        } catch (UserNotOwnerException e) {
            responseObserver.onError(FAILED_PRECONDITION.asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [SHARE_LIST] [ERROR] " + e.getMessage());
        }
    }

    @Override
    public void unshareList(UnshareListRequest request, StreamObserver<UnshareListResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [UNSHARE_LIST] [STARTED]");

        try {
            server.unshareList(request.getUsernameUnsharing(), request.getUsernameToUnshare(), request.getListId().getValue());

            responseObserver.onNext(UnshareListResponse.getDefaultInstance());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [UNSHARE_LIST] [FINISHED]");
        } catch (InvalidUserException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("User is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [UNSHARE_LIST] [ERROR] " + e.getMessage());
        } catch (InvalidListException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("List is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [UNSHARE_LIST] [ERROR] " + e.getMessage());
        } catch (UserNotOwnerException e) {
            responseObserver.onError(FAILED_PRECONDITION.asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [UNSHARE_LIST] [ERROR] " + e.getMessage());
        }
    }

    @Override
    public void removeItemFromList(RemoveItemRequest request, StreamObserver<RemoveItemResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [STARTED]");
        try {
            server.removeItemFromList(request.getUsername(), request.getListId().getValue(), request.getItemName());
            responseObserver.onNext(RemoveItemResponse.getDefaultInstance());
            LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [FINISHED]");
        } catch (UserNotOwnerException e) {
            responseObserver.onError(FAILED_PRECONDITION.asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [ERROR] " + e.getMessage());
        } catch (InvalidListException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("List is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [ERROR] " + e.getMessage());
        } catch (InvalidUserException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("User is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [ERROR] " + e.getMessage());
        } catch (InvalidItemException e) {
            responseObserver.onError(INVALID_ARGUMENT.withDescription("Item is invalid").asRuntimeException());
            LOGGER.info("[ " + (new Date()).toString() + " ] [REMOVE_ITEM_FROM_LIST] [ERROR] " + e.getMessage());
        }

        responseObserver.onCompleted();
    }

    //
    // Item-related services
    //
    @Override
    public void createItem(CreateItemRequest request, StreamObserver<CreateItemResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_ITEM] [STARTED]");
        try {
            server.createItem(request.getName(), request.getBarcode());

            responseObserver.onNext(CreateItemResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_ITEM] [FINISHED] ");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_ITEM] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void addItemPicture(AddItemPictureRequest request, StreamObserver<AddItemPictureResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_PICTURE] [STARTED]");

        try {
            server.addItemPicture(request.getName(), request.getPicture());

            responseObserver.onNext(AddItemPictureResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_PICTURE] [FINISHED] ");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_PICTURE] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void addItemsToStore(AddItemsToStoreRequest request, StreamObserver<AddItemsToStoreResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_STORE] [STARTED]");

        try {

            for (Shop.Item item: request.getItemsList()) {
                server.addItemStore(item.getName(), request.getStore(), item.getPrice());
            }

            responseObserver.onNext(AddItemsToStoreResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_STORE] [FINISHED] ");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [ADD_ITEM_PICTURE] [ERROR]" + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void updateItemAmount(UpdateItemAmountRequest request, StreamObserver<UpdateItemAmountResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_QUANTITY] [STARTED]");

        try {
            server.updateItemAmount(request.getListId().getValue(), request.getItemName(), request.getPantryAmount(), request.getShopAmount());

            responseObserver.onNext(UpdateItemAmountResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_QUANTITY] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_QUANTITY] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void updateItemDetails(UpdateItemDetailsRequest request, StreamObserver<UpdateItemDetailsResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_DETAILS] [STARTED]");

        try {
            server.updateItemDetails(request.getListId().getValue(), request.getOldItemName(), request.getNewItemName(), request.getBarcode(), request.getPantryAmount(), request.getShopAmount());

            responseObserver.onNext(UpdateItemDetailsResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_DETAILS] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_DETAILS] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void rateItem(RateItemRequest request, StreamObserver<RateItemResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [RATE_ITEM] [STARTED]");

        try {
            server.rateItem(request.getUsername(), request.getItem(), request.getRating());

            responseObserver.onNext(RateItemResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [RATE_ITEM] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [RATE_ITEM] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void canRateItem(CanRateItemRequest request, StreamObserver<CanRateItemResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [] [STARTED]");

        try {
            Boolean canRate = server.canRate(request.getUsername(), request.getItem());

            responseObserver.onNext(CanRateItemResponse.newBuilder()
                    .setCanRate(canRate)
                    .build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEM_RATING] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEM_RATING] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getItemRating(GetItemRatingRequest request, StreamObserver<GetItemRatingResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEM_RATING] [STARTED]");

        try {
            Collection<Integer> ratings = server.getItemRating(request.getItem());

            responseObserver.onNext(GetItemRatingResponse.newBuilder()
                    .addAllRatings(ratings)
                    .build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEM_RATING] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEM_RATING] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void createStore(CreateStoreRequest request, StreamObserver<CreateStoreResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_STORE] [STARTED]");

        try {
            server.createStore(request.getName(), request.getLatitude(), request.getLongitude());
            for (Shop.Item item: request.getItemList()) {
                server.addItemStore(item.getName(), request.getName(), item.getPrice());
            }

            responseObserver.onNext(CreateStoreResponse.getDefaultInstance());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_STORE] [FINISHED] " + "[STORE: " + request.getName() + "]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [CREATE_STORE] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getStores(GetStoresRequest request, StreamObserver<GetStoresResponse> responseObserver) {
        LOGGER.info("[STARTED] GetStores");

        try {
            Map<Store, Long> stores = server.getStores(request.getUsername());
            GetStoresResponse.Builder response = GetStoresResponse.newBuilder();

            for (Map.Entry<Store, Long> entry : stores.entrySet()) {
                Store store = entry.getKey();
                String name = store.getName();
                Coordinates coordinates = store.getLocation();

                response.addStores(Shop.Store.newBuilder()
                        .setName(name)
                        .setLatitude(coordinates.getLatitude())
                        .setLongitude(coordinates.getLongitude())
                        .setWaitTime(entry.getValue())
                        .build());
            }

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

            LOGGER.info("[FINISHED] GetStores");
        } catch (Exception e) {
            LOGGER.info("[ERROR] GetStores - " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getStoreView(GetStoreViewRequest request, StreamObserver<GetStoreViewResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [GET_STORE_VIEW] [STARTED]");

        try {
            Set<StoreItem> item_list = server.getStoreView(request.getUsername(), request.getStoreName());
            GetStoreViewResponse.Builder response = GetStoreViewResponse.newBuilder();

            // Iterates through all items in the item list to create respective DTOs
            for (StoreItem store_item : item_list) {
                Item item = server.getItem(store_item.getName());
                LOGGER.info("item " + store_item);
                String name = item.getName();
                String barcode = item.getBarcode();
                List<String> pictures = item.getPictures();
                int totalShopAmount = server.getAllShopAmount(request.getUsername(), name);
                if (totalShopAmount == 0) {
                    continue;
                }
                Double price = store_item.getPrice();
                String frequentlyBoughtWith = store_item.frequentlyBoughtTogether();

                response.addItems(Shop.Item.newBuilder()
                        .setName(name)
                        .setBarcode(barcode)
                        .setShopAmount(totalShopAmount)
                        .setPrice(price)
                        .addAllPictures(pictures)
                        .setFrequentlyBoughtWith(frequentlyBoughtWith)
                        .build());
            }

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_STORE_VIEW] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_STORE_VIEW] [ERROR] " + e);
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getAllItems(GetAllItemsRequest request, StreamObserver<GetAllItemsResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ALL_ITEMS] [STARTED]");

        try {
            Collection<Item> items = server.getAllItems();
            GetAllItemsResponse.Builder response = GetAllItemsResponse.newBuilder();

            // Iterates through all items in the item list to create respective DTOs
            for (Item item : items) {
                String name = item.getName();
                String barcode = item.getBarcode();
                List<String> pictures = item.getPictures();

                response.addItems(Shop.Item.newBuilder()
                        .setName(name)
                        .setBarcode(barcode)
                        .addAllPictures(pictures)
                        .build());
            }

            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ALL_ITEMS] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ALL_ITEMS] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void checkoutStore(CheckoutStoreRequest request, StreamObserver<CheckoutStoreResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [CHECKOUT_STORE] [STARTED]");

        try {
            String username = request.getUsername();
            String store = request.getStore();
            List<Shop.Item> items = request.getItemsList();

            for (int i = 0; i < items.size(); i++) {
                server.buyItem(username, items.get(i).getName(), items.get(i).getShopAmount());
                for (int j = i + 1; j < items.size(); j++) {
                    server.updateStore(store, items.get(i).getName(), items.get(j).getName());
                }
            }

            responseObserver.onNext(CheckoutStoreResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [CHECKOUT_STORE] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [CHECKOUT_STORE] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void getItemsNotInStore(GetItemsNotInStoreRequest request, StreamObserver<GetItemsNotInStoreResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEMS_NOT_IN_STORE] [STARTED]");

        try {
            Collection<String> items = server.getItemsNotInStore(request.getStore());

            responseObserver.onNext(GetItemsNotInStoreResponse.newBuilder().addAllItemName(items).build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEMS_NOT_IN_STORE] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [GET_ITEMS_NOT_IN_STORE] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void updateItemPrice(UpdateItemPriceRequest request, StreamObserver<UpdateItemPriceResponse> responseObserver) {
        LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_PRICE] [STARTED]");

        try {
            server.updateItemPrice(request.getStore(), request.getItem(), request.getPrice());

            responseObserver.onNext(UpdateItemPriceResponse.newBuilder().build());
            responseObserver.onCompleted();

            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_PRICE] [FINISHED]");
        } catch (Exception e) {
            LOGGER.info("[ " + (new Date()).toString() + " ] [UPDATE_ITEM_PRICE] [ERROR] " + e.getMessage());
            responseObserver.onError(INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
        }
    }

    @Override
    public void enterQueue(EnterQueueRequest request, StreamObserver<EnterQueueResponse> responseObserver) {
        LOGGER.info("[ENTER_QUEUE] [STARTED] User: " + request.getUsername() + " Store: " + request.getStore());
        server.addUserToQueue(request.getStore(), request.getUsername(), request.getNumItems());
        responseObserver.onNext(EnterQueueResponse.getDefaultInstance());
        responseObserver.onCompleted();
        LOGGER.info("[ENTER_QUEUE] [FINISHED] User: " + request.getUsername() + " Store: " + request.getStore());
    }

    @Override
    public void leaveQueue(LeaveQueueRequest request, StreamObserver<LeaveQueueResponse> responseObserver) {
        LOGGER.info("[LEAVE_QUEUE] [STARTED] User: " + request.getUsername() + " Store: " + request.getStore());
        server.removeUserFromQueue(request.getStore(), request.getUsername());
        responseObserver.onNext(LeaveQueueResponse.getDefaultInstance());
        responseObserver.onCompleted();
        LOGGER.info("[LEAVE_QUEUE] [FINISHED] User: " + request.getUsername() + " Store: " + request.getStore());
    }
}