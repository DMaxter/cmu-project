package pt.ulisboa.tecnico.cmu.server.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Item implements java.io.Serializable {
    private String name;
    private String barcode;
    private List<String> pictures = new ArrayList<String>();
    private HashMap<String, Integer> ratings = new HashMap<>();

    public Item(String name, String barcode) {
        this.name = name;
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void addPicture(String picture) {
        pictures.add(picture);
    }

    public String getBarcode() {
        return barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void rateItem(String username, int rating) {
        ratings.put(username, rating);
    }

    public Collection<Integer> getRatings() {
        return ratings.values();
    }

    public boolean canRateItem(String username) {
        return !ratings.containsKey(username);
    }
}
