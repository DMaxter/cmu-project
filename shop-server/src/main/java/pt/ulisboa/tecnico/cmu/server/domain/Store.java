package pt.ulisboa.tecnico.cmu.server.domain;

import pt.ulisboa.tecnico.cmu.server.domain.exceptions.InvalidLocationException;

import java.util.*;

public class Store implements java.io.Serializable {
    private final String name;
    private final Coordinates coords;
    private HashMap<String, StoreItem> items = new HashMap<>();      //<name, price>

    public Store(String name, Coordinates coords) throws InvalidLocationException {
        if (coords == null) {
            throw new InvalidLocationException();
        }

        this.name = name;
        this.coords = coords;
    }

    public void addItem(String name, Double price) {
        for (StoreItem item: items.values()) {
            item.addOtherStoreItem(name);
        }

        items.put(name, new StoreItem(name, price, items.keySet()));
    }

    public void removeItem(String name) {
        items.remove(name);
    }

    public double getPrice(String name) {
        return items.get(name).getPrice();
    }

    public Coordinates getLocation() {
        return this.coords;
    }

    public String getName() {
        return name;
    }

    public Set<StoreItem> getPantryItemsInStore(Set<String> items_in_pantries) {

        Set<StoreItem> pantryItemsInStore = new TreeSet<>();

        for (StoreItem item: items.values()) {
            if (items_in_pantries.contains(item.getName())) {
                pantryItemsInStore.add(item);
            }
        }

        return pantryItemsInStore;
    }

    public Collection<String> getItemsNotInStore(Set<String> items_in_store) {
        Set<String> items_not_in_store = new HashSet<>(items_in_store);

        for (String item_name: items.keySet()) {
            items_not_in_store.remove(item_name);
        }

        return items_not_in_store;
    }

    public void updateItemPrice(String item_name, double price) {
        if (items.containsKey(item_name)) {
            items.get(item_name).setPrice(price);
        }
        else {
            addItem(item_name, price);
        }
    }

    public StoreItem getItem(String name) {
        return items.get(name);
    }
    /*public Map<String, Double> getAllItemsRaw() {
        return this.priceList;
    }*/
}