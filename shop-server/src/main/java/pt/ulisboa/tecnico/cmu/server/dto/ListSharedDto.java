package pt.ulisboa.tecnico.cmu.server.dto;

import java.util.Set;
import java.util.TreeSet;

public class ListSharedDto {
    private final TreeSet<String> users;

    public ListSharedDto(Set<String> users) {
        this.users = new TreeSet<>(users);
    }

    public TreeSet<String> getUsers() {
        return users;
    }
}
